<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class JQueryUIAsset extends AssetBundle
{
    public $sourcePath = '@webroot/backoffice';
    public $baseUrl = '@web/backoffice';
    public $css = [
        'css/jquery-ui/jquery-ui.min.css',
        'css/jquery-ui/jquery-ui.structure.min.css',
        'css/jquery-ui/jquery-ui.theme.min.css',
    ];
    public $js = [
        'js/jquery-ui.min.js',
    ];
    public $depends = [
        JqueryAsset::class,
    ];
}
