<?php

namespace app\assets\backoffice;

use app\widgets\assets\GridViewAsset;
use yii\web\AssetBundle;
use app\assets\AdminLteAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BackofficeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        AdminLteAsset::class,
        GridViewAsset::class,
    ];
}
