<?php

namespace app\assets\backoffice;

use yii\web\AssetBundle;

class BackofficeLoginAsset extends AssetBundle
{
    public $sourcePath = '@app/web/backoffice';
    public $js = [
        'js/login.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        BackofficeAsset::class,
    ];
}