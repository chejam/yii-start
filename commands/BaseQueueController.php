<?php

namespace app\commands;

use app\components\RabbitQueue;
use app\models\queueMessages\BaseQueueMessage;
use yii\base\InvalidConfigException;
use yii\console\Controller;

abstract class BaseQueueController extends Controller
{
    public $defaultAction = 'process';
    public $amount = 100;

    protected $class;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (empty($this->class)) {
            throw new InvalidConfigException('Class property must be set');
        }
    }

    public function actionProcess()
    {
        foreach ($this->getQueuedMessages() as $message) {
            $message->process();
        }
    }

    /**
     * @return BaseQueueMessage[]
     */
    protected function getQueuedMessages() : array
    {
        /** @var RabbitQueue $rabbitQueue */
        $rabbitQueue = \Yii::$app->rabbit;
        $messages = [];
        for ($i = 0; $i < $this->amount; ++$i) {
            $message = $rabbitQueue->pop(new $this->class);
            if (empty($message)) {
                return $messages;
            }
            $messages[] = $message;
        }
        return $messages;
    }
}