<?php

namespace app\commands;

use app\models\queueMessages\EmailMessage;

class EmailController extends BaseQueueController
{
    protected $class = EmailMessage::class;
}