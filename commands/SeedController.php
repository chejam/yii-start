<?php

namespace app\commands;

use app\components\seeders\BaseSeeder;
use app\components\seeders\PermissionsSeeder;
use app\components\seeders\RolesPermissionsSeeder;
use app\components\seeders\RolesSeeder;
use app\components\seeders\UsersRolesSeeder;
use app\components\seeders\UsersSeeder;
use yii\base\InvalidConfigException;
use yii\console\Controller;

class SeedController extends Controller
{
    public $defaultAction = 'run';

    public $seederList = [
        RolesSeeder::class,
        PermissionsSeeder::class,
        RolesPermissionsSeeder::class,
        UsersSeeder::class,
        UsersRolesSeeder::class,
    ];

    public function actionStart()
    {
        foreach ($this->seederList as $seederName) {
            $seeder = $this->getSeeder($seederName);
            $seeder->seed();
        }
    }

    public function actionRun(array $seeder = [])
    {
        if (empty($seeder)) {
            echo "В качестве параметра укажите номер или имя\n";
            foreach ($this->seederList as $key => $seederName) {
                echo $key . ") " . $seederName . "\n";
            }
        } else {
            foreach ($seeder as $seederOne) {
                $runSeeder = false;
                if (is_numeric($seederOne) && isset($this->seederList[$seederOne])) {
                    $runSeeder = $this->getSeeder($this->seederList[$seederOne]);
                } else if (is_string($seederOne) && in_array($seederOne, $this->seederList)) {
                    $runSeeder = $this->getSeeder($seederOne);
                }
                if ($runSeeder !== false) {
                    $runSeeder->seed();
                }
            }
        }
    }

    /**
     * @param string $name
     * @return BaseSeeder
     * @throws InvalidConfigException
     */
    private function getSeeder(string $name) : BaseSeeder
    {
        $seeder = new $name;
        if (!$seeder instanceof BaseSeeder) {
            throw new InvalidConfigException("Seeder $name not instance of app\\components\\seeders\\BaseSeeder");
        }
        return $seeder;
    }

}