<?php

namespace app\components;

use app\models\UserRole;
use yii\base\Object;
use yii\rbac\CheckAccessInterface;

class AccessChecker extends Object implements CheckAccessInterface
{
    private $userPermissions = null;

    /**
     * @inheritdoc
     */
    public function checkAccess($userId, $permissionName, $params = []) : bool
    {
        if ($userId) {
            $permissions = $this->getCurrentUserPermissions($userId);
            return isset($permissions[$permissionName]);
        } else {
            return false;
        }
    }


    private function getCurrentUserPermissions(int $userId) : array
    {
        if (!isset($this->userPermissions[$userId])) {
            $this->userPermissions[$userId] = UserRole::find()->findPermissions($userId);
        }
        return $this->userPermissions[$userId];
    }

//    /**
//     * @param int $userId
//     * @return Permission[]
//     */
//    private function getCurrentUserPermissions(int $userId) : array
//    {
//        if ($this->currentUserPermissions === null) {
//            $sqlSpacePermissions = \app\models\User::find()
//                ->joinWith(['spaces', 'spaces.spacePermissions'])
//                ->select('space_permissions_status.permission_name')
//                ->andWhere(['users.id' => $userId, 'users.type_id' => UserType::TYPE_SPACE])
//                ->createCommand()->rawSql;
//
//            $this->currentUserPermissions = Permission::find()
//                ->joinWith(['roles.users'])
//                ->andWhere(['<>', 'users.type_id', UserType::TYPE_SPACE])
//                ->orWhere('permissions.name IN (' . $sqlSpacePermissions . ')')
//                ->andWhere(['users.id' => $userId])
//                ->indexBy('name')
//                ->all();
//        }
//        return $this->currentUserPermissions;
//    }
}