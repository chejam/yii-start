<?php

namespace app\components;

use app\models\queueMessages\BaseQueueMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use yii\base\InvalidConfigException;
use yii\base\Object;

class RabbitQueue extends Object
{
    /** @var AMQPChannel[] */
    private $_channels;

    /** @var string */
    public $host = 'localhost';
    /** @var int */
    public $port = 5672;
    /** @var string */
    public $user;
    /** @var string */
    public $password;
    /** @var string */
    public $vhost = '/';

    /** @var AMQPStreamConnection */
    protected static $amqpConnection;

    public function init()
    {
        if (empty($this->user)) {
            throw new InvalidConfigException('User property must be set');
        }
        if (empty(self::$amqpConnection)) {
            self::$amqpConnection = new AMQPStreamConnection(
                $this->host,
                $this->port,
                $this->user,
                $this->password,
                $this->vhost
            );
        }
    }

    /**
     * @param null $channelId
     * @return AMQPChannel
     */
    private function getChannel($channelId = null) : AMQPChannel
    {
        if (empty($this->_channels[$channelId])) {
            $this->_channels[$channelId] = self::$amqpConnection->channel($channelId);
        }
        return $this->_channels[$channelId];
    }

    /**
     * @param BaseQueueMessage $message
     * @param array $properties
     */
    public function push(BaseQueueMessage $message, array $properties = [])
    {
        $this->prepareExchange($message);
        $amqpMessage = $this->prepareMessage($message, $properties);
        $this->getChannel()->basic_publish($amqpMessage, $message->getExchange(), $message->getRoutingKey());
    }

    /**
     * @param BaseQueueMessage $message
     * @return BaseQueueMessage|null
     */
    public function pop(BaseQueueMessage $message)
    {
        $this->prepareExchange($message);
        $message = $this->getChannel()->basic_get($message::getQueue(), true);
        if ($message) {
            return $this->populateMessage($message);
        }
        return null;
    }

    /**
     * @param BaseQueueMessage $message
     */
    private function prepareExchange(BaseQueueMessage $message)
    {
        $channel = $this->getChannel();
        $channel->queue_declare($message::getQueue(), false, true, false, false);
        $channel->exchange_declare($message::getExchange(), 'direct', false, true, false);
        $channel->queue_bind($message::getQueue(), $message::getExchange(), $message::getRoutingKey());
    }

    /**
     * @param BaseQueueMessage $message
     * @param array $properties
     * @return AMQPMessage
     */
    private function prepareMessage(BaseQueueMessage $message, array $properties = []) : AMQPMessage
    {
        return new AMQPMessage(json_encode(
            [
                'class' => get_class($message),
                'data' => $message->exportToJson(),
            ]
        ), $properties);
    }

    /**
     * @param AMQPMessage|string $message
     * @return BaseQueueMessage|null
     */
    private function populateMessage(AMQPMessage $message)
    {
        $data = json_decode($message->body, true);
        if (isset($data['class']) && isset($data['data'])) {
            if (class_exists($data['class'])) {
                $class = $data['class'];
                return $class::loadFromJson($data['data']);
            }
        }
        return null;
    }


}