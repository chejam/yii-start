<?php

namespace app\components;

use yii\rbac\CheckAccessInterface;

class User extends \yii\web\User
{
    public function __construct(CheckAccessInterface $accessChecker, array $config = [])
    {
        parent::__construct($config);
        $this->accessChecker = $accessChecker;
    }
}