<?php

namespace app\components\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class StringModifyBehavior extends Behavior
{
    const LOWER_CASE_MODIFIER = 'toLower';
    /** @var string[] */
    public $stringFields = [];
    public $modifier = self::LOWER_CASE_MODIFIER;

    /**
     * @return array
     */
    public function events() : array
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'onBeforeValidate',
        ];
    }

    /**
     * @param $event
     */
    public function onBeforeValidate($event)
    {
        foreach ($this->stringFields as $field) {
            if (isset($this->owner->$field)) {
                $this->owner->$field = call_user_func([$this, $this->modifier], $this->owner->$field);
            }
        }
    }

    /**
     * @param string $string
     * @return string
     */
    private function toLower(string $string) : string
    {
        return mb_convert_case($string, MB_CASE_LOWER , "UTF-8");
    }
}