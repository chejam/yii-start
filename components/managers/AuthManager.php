<?php

namespace app\components\managers;

use app\components\managers\interfaces\AuthManagerInterface;
use app\models\AuthCode;
use app\models\PasswordResetCode;
use yii\base\Object;

class AuthManager extends Object implements AuthManagerInterface
{
    /**
     * @param int $userId
     * @return AuthCode
     */
    public function generateAuthCode(int $userId) : AuthCode
    {
        $authCode = new AuthCode();
        $authCode->user_id = $userId;
        $authCode->ttl = time() + \Yii::$app->params['auth']['ttl'];
        $authCode->auth_code = \Yii::$app->security->generateRandomString();
        $authCode->original_user_id = \Yii::$app->user->id;
        $authCode->save();
        return $authCode;
    }

    /**
     * @param string $authCode
     * @return bool
     */
    public function authByCode(string $authCode) : bool
    {
        $authCode = AuthCode::find()->byAuthCode($authCode)->checkTtl()->one();
        if ($authCode !== null && $authCode->user !== null) {
            return \Yii::$app->user->login($authCode->user);
        }
        return false;
    }

    /**
     * @param int $userId
     * @return PasswordResetCode
     */
    public function generateResetPasswordToken(int $userId) : PasswordResetCode
    {
        $passwordResetCode = new PasswordResetCode();
        $passwordResetCode->user_id = $userId;
        $passwordResetCode->ttl = \Yii::$app->params['auth']['ttl'] + time( );
        $passwordResetCode->token = \Yii::$app->security->generateRandomString(24) . $userId;
        $passwordResetCode->save();
        return $passwordResetCode;
    }

    /**
     * @param string $hash
     * @return PasswordResetCode|null
     */
    public function resetPassword(string $hash)
    {
        $userId = (int) substr($hash, 24);
        if ($userId) {
            /** @var PasswordResetCode $passwordResetCode */
            $passwordResetCode = PasswordResetCode::find()->byUser($userId)->byToken($hash)->checkTtl()->one();
            return $passwordResetCode;
        }
        return null;
    }
}