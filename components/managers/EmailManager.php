<?php

namespace app\components\managers;

use app\components\managers\interfaces\EmailManagerInterface;
use app\models\queueMessages\EmailMessage;
use yii\base\Object;

class EmailManager extends Object implements EmailManagerInterface
{
    /**
     * @inheritdoc
     */
    public function sendMessageInstant(string $recipient, string $topic, string $view, array $params = []) : bool
    {
        return \Yii::$app->mailer->compose($view, $params)
            ->setFrom(\Yii::$app->params['email']['from'])
            ->setTo($recipient)
            ->setSubject($topic)
            ->send();
    }

    /**
     * @inheritdoc
     */
    public function sendPlainTextMessageInstant(string $recipient, string $topic, string $message) : bool
    {
        return \Yii::$app->mailer->compose()
            ->setFrom(\Yii::$app->params['email']['from'])
            ->setTo($recipient)
            ->setSubject($topic)
            ->setTextBody($message)
            ->send();
    }

    /**
     * @inheritdoc
     */
    public function sendMessage($recipient, string $topic, string $view, array $params = []) : bool
    {
        $hasErrors = false;
        $recipients = is_array($recipient) ? $recipient : [$recipient];
        foreach ($recipients as $recipient) {
            $emailMessage = new EmailMessage();
            $emailMessage->setAttributes([
                'recipient' => $recipient,
                'topic' => $topic,
                'view' => $view,
                'params' => $params,
            ]);
            $emailMessage->push();
            $hasErrors |= $emailMessage->hasErrors();
        }
        return !$hasErrors;
    }

    /**
     * @inheritdoc
     */
    public function sendPlainTextMessage(string $recipient, string $topic, string $message) : bool
    {
        $emailMessage = new EmailMessage();
        $emailMessage->setAttributes([
            'recipient' => $recipient,
            'topic' => $topic,
            'message' => $message,
        ]);
        $emailMessage->push();
        return !$emailMessage->hasErrors();
    }
}