<?php

namespace app\components\managers;

use app\components\managers\interfaces\UserGridViewSettingsManagerInterface;
use app\models\UserGridViewSettings;
use yii\base\Object;

class UserGridViewSettingsManager extends Object implements UserGridViewSettingsManagerInterface
{
    /**
     * @inheritdoc
     */
    public function getSettings(int $userId, string $route, string $gridId) : array
    {
        $settings = $this->getSettingsAR($userId, $route, $gridId);
        return $settings ? json_decode($settings->params, true)['settings'] : [];
    }

    /**
     * @inheritdoc
     */
    public function setSettings(int $userId, string $route, string $gridId, array $params) : UserGridViewSettings
    {
        $settings = $this->getSettingsAR($userId, $route, $gridId);
        if ($settings === null) {
            $settings = new UserGridViewSettings();
            $settings->user_id = $userId;
            $settings->route = $route;
            $settings->grid_id = $gridId;
        }
        $settings->params = json_encode(['settings' => $this->prepareParams($params)]);
        $settings->save();
        return $settings;
    }

    /**
     * @param int $userId
     * @param string $route
     * @param string $gridId
     * @return UserGridViewSettings|null
     */
    private function getSettingsAR(int $userId, string $route, string $gridId)
    {
        return UserGridViewSettings::find()->byRoute($route)->byGridId($gridId)->byUserId($userId)->one();
    }

    /**
     * @param array $params
     * @return array
     */
    private function prepareParams(array $params) : array
    {
        $preparedParams = [];
        foreach ($params as $name => $value) {
            $settings['name'] = $name;
            $settings['visible'] = (bool)$value;
            if (!is_numeric($value)) {
                $settings['value'] = $value;
            }
            $preparedParams[] = $settings;
        }
        return $preparedParams;
    }

    /**
     * @inheritdoc
     */
    public function removeSettings(int $userId) : bool
    {
        return UserGridViewSettings::deleteAll(['user_id' => $userId]);
    }
}