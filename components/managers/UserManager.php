<?php

namespace app\components\managers;

use app\components\managers\interfaces\UserManagerInterface;
use app\models\company\Company;
use app\models\space\Space;
use app\models\Tmc;
use app\models\User;
use app\models\UserType;
use yii\base\Object;

class UserManager extends Object implements UserManagerInterface
{
    /**
     * @inheritdoc
     */
    private function canChangeStatus(string $oldStatus, string $newStatus) : bool
    {
        return $oldStatus != $newStatus;
    }

    /**
     * @inheritdoc
     */
    public function changeStatus(User $user, string $newStatus) : bool
    {
        $oldStatus = $user->status;
        if ($this->canChangeStatus($oldStatus, $newStatus)) {
            $user->status = $newStatus;
            return $user->save();
        }
        return false;
    }

}