<?php

namespace app\components\managers\interfaces;

use app\models\AuthCode;
use app\models\PasswordResetCode;

interface AuthManagerInterface
{
    /**
     * @param int $userId
     * @return AuthCode
     */
    public function generateAuthCode(int $userId) : AuthCode;

    /**
     * @param string $authCode
     * @return bool
     */
    public function authByCode(string $authCode) : bool;

    /**
     * @param int $userId
     * @return PasswordResetCode
     */
    public function generateResetPasswordToken(int $userId) : PasswordResetCode;

    /**
     * @param string $hash
     * @return PasswordResetCode|null
     */
    public function resetPassword(string $hash);
}