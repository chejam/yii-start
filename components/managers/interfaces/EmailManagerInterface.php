<?php

namespace app\components\managers\interfaces;

interface EmailManagerInterface
{
    /**
     * @param string|string[] $recipient
     * @param string $topic
     * @param string $view
     * @param array $params
     * @return bool
     */
    public function sendMessage($recipient, string $topic, string $view, array $params = []) : bool;

    /**
     * @param string $recipient
     * @param string $topic
     * @param string $message
     * @return bool
     */
    public function sendPlainTextMessage(string $recipient, string $topic, string $message) : bool;

    /**
     * @param string $recipient
     * @param string $topic
     * @param string $view
     * @param array $params
     * @return bool
     */
    public function sendMessageInstant(string $recipient, string $topic, string $view, array $params = []) : bool;

    /**
     * @param string $recipient
     * @param string $topic
     * @param string $message
     * @return bool
     */
    public function sendPlainTextMessageInstant(string $recipient, string $topic, string $message) : bool;
}