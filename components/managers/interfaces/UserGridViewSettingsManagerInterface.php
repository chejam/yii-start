<?php

namespace app\components\managers\interfaces;

use app\models\UserGridViewSettings;

interface UserGridViewSettingsManagerInterface
{
    /**
     * @param int $userId
     * @param string $route
     * @param string $gridId
     * @param array $params
     * @return UserGridViewSettings
     */
    public function setSettings(int $userId, string $route, string $gridId, array $params) : UserGridViewSettings;

    /**
     * @param int $userId
     * @param string $route
     * @param string $gridId
     * @return array
     */
    public function getSettings(int $userId, string $route, string $gridId) : array;

    /**
     * @param int $userId
     * @return bool
     */
    public function removeSettings(int $userId) : bool;
}