<?php

namespace app\components\managers\interfaces;

use app\models\User;

interface UserManagerInterface
{
    /**
     * @param User $user
     * @param string $newStatus
     * @return bool
     */
    public function changeStatus(User $user, string $newStatus) : bool;

}