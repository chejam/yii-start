<?php

namespace app\components\seeders;

use yii\base\InvalidConfigException;
use yii\base\Object;
use yii\db\ActiveRecord;

/**
 * Class BaseSeeder
 * @package app\components\seeders
 */
abstract class BaseSeeder extends Object
{
    /** @var array */
    protected $data = null;
    /** @var ActiveRecord */
    protected $modelClass = null;
    /** @var string */
    protected $tableName = null;
    /** @var string */
    protected $attributesList = null;
    /** @var bool */
    protected $appendCreateTime = false;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->data = $this->initData();
        if ($this->data === null) {
            throw new InvalidConfigException('The "data" property must be set.');
        }
        if ($this->modelClass === null) {
            if ($this->tableName === null && $this->attributesList === null) {
                throw new InvalidConfigException('The "modelClass" property or pair of "attributesList"&"tableName" must be set.');
            }
        } else {
            $modelClass = $this->modelClass;
            $this->tableName = $modelClass::tableName();
            $this->attributesList = array_diff($modelClass::getTableSchema()->getColumnNames(), (array)$modelClass::getTableSchema()->primaryKey);
        }
    }

    /**
     * @return bool
     */
    public function seed() : bool
    {
        $data = $this->data;
        if ($this->appendCreateTime) {
            $currentTime = time();
            foreach ($data as &$objectParams) {
                $objectParams['create_time'] = $currentTime;
                $objectParams['update_time'] = $currentTime;
            }
            unset($objectParams);
        }

        \Yii::$app->db->createCommand(\Yii::$app->db->createCommand()->truncateTable($this->tableName)->getRawSql() . ' cascade')->execute();
        $schema = \Yii::$app->db->getTableSchema($this->tableName);
        if ($schema->sequenceName !== null) {
            \Yii::$app->db->createCommand()->resetSequence($this->tableName)->execute();
        }
        $affectedRows = \Yii::$app->db->createCommand()
            ->batchInsert($this->tableName, $this->attributesList, $data)->execute();
        return $affectedRows == count($this->data);
    }

    /**
     * @return array
     */
    protected function initData() : array
    {
        return $this->data;
    }
}