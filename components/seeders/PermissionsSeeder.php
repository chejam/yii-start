<?php

namespace app\components\seeders;

use app\models\Permission;

class PermissionsSeeder extends BaseSeeder
{
    public $modelClass = Permission::class;

    /**
     * @return array
     */
    protected function initData() : array
    {
        $data = [];
        $groupPermissions = Permission::permissionsByType();
        foreach ($groupPermissions as $entityType => $permissions) {
            foreach ($permissions as $permission => $label) {
                $data[] = [
                    'entity_type' => $entityType,
                    'name'        => $permission,
                    'label'       => $label,
                ];
            }
        }
        return $data;
    }
}