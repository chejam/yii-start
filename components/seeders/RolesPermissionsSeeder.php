<?php

namespace app\components\seeders;

use app\models\Permission;
use app\models\Role;

class RolesPermissionsSeeder extends BaseSeeder
{
    protected $tableName = 'roles_permissions';
    protected $attributesList = ['role_id', 'permission_id'];

    protected $managerExcludePermissions = [
        Permission::BACKOFFICE_USER_CREATE,
        Permission::BACKOFFICE_USER_DELETE,
        Permission::BACKOFFICE_USER_UPDATE,
        Permission::BACKOFFICE_USER_VIEW,
    ];

    /**
     * @return array
     */
    protected function initData() : array
    {
        $data = [];
        $groupPermissions = Permission::permissionsByType();
        $counterRoles = 0;
        foreach ($groupPermissions as $entityType => $permissionsList) {
            $firstRoles = Role::find()->where(['entity_type' => $entityType])->limit(2)->all();
            if ($firstRoles) {
                $permissions = Permission::find()->byType($entityType)->all();
                foreach ($firstRoles as $firstRole) {
                    $counterRoles++;
                    foreach ($permissions as $permission) {
                        if ($counterRoles == 1) {
                            $data[] = [
                                'role_id' => $firstRole->id,
                                'permission_id' => $permission->id,
                            ];
                        }
                        if ($counterRoles == 2 && !in_array($permission->name, $this->managerExcludePermissions)) {
                            $data[] = [
                                'role_id' => $firstRole->id,
                                'permission_id' => $permission->id,
                            ];
                        }
                    }
                }
            }
        }
        return $data;
    }
}