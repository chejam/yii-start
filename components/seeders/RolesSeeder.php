<?php

namespace app\components\seeders;

use app\models\Role;
use app\models\User;

class RolesSeeder extends BaseSeeder
{
    public $modelClass = Role::class;
    public $data = [
        ['entity_type' => Role::ROLE_TYPE_BACKOFFICE, 'entity_id' => null, 'name' => 'Администратор', 'create_time' => null, 'update_time' => null],
        ['entity_type' => Role::ROLE_TYPE_BACKOFFICE, 'entity_id' => null, 'name' => 'Менеджер', 'create_time' => null, 'update_time' => null],
        ['entity_type' => Role::ROLE_TYPE_USER, 'entity_id' => null, 'name' => 'Пользователь', 'create_time' => null, 'update_time' => null],
    ];
}