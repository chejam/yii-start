<?php

namespace app\components\seeders;

use app\models\Permission;
use app\models\Role;
use app\models\User;
use app\models\UserRole;

class UsersRolesSeeder extends BaseSeeder
{
    public $modelClass = UserRole::class;

    /**
     * @return array
     */
    protected function initData() : array
    {
        $data = [];
        $groupPermissions = Permission::permissionsByType();
        $firstUser = User::find()->active()->one();
        if ($firstUser) {
            foreach ($groupPermissions as $entityType => $permissionsList) {
                $firstRole = Role::findOne(['entity_type' => $entityType]);
                if ($firstRole) {
                    $data[] = [
                        'user_id'     => $firstUser->id,
                        'role_id'     => $firstRole->id,
                        'entity_type' => $entityType,
                        'entity_id'   => null,
                        'create_time' => null,
                        'update_time' => null,
                    ];
                }
            }
        }
        return $data;
    }
}