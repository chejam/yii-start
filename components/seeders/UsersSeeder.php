<?php

namespace app\components\seeders;

use app\models\User;

class UsersSeeder extends BaseSeeder
{
    protected $modelClass = User::class;
    protected $appendCreateTime = true;

    protected function initData() : array
    {
        return [
            [
                'username'             => 'Администратор',
                'email'                => 'admin@example.com',
                'surname'              => 'Иванов',
                'name'                 => 'Иван',
                'patronymic'           => 'Иванович',
                'phone1'               => '8',
                'phone2'               => '8',
                'phone2_ext'           => '0',
                'auth_key'             => \Yii::$app->security->generateRandomString(),
                'password_hash'        => \Yii::$app->security->generatePasswordHash('admin'),
                'password_reset_token' => null,
                'status'               => User::STATUS_ACTIVATED,
                'create_time'          => null,
                'update_time'          => null,
            ],
        ];
    }
}