<?php

$backoffice_params = [
    'defaultRoute' => 'backoffice/site/index',
    'modules' => [
        'backoffice' => [
            'class' => 'app\modules\backoffice\Backoffice',
            'layout' => 'main',
        ],
    ],
    'components' => [
        'session' => [
            'name' => 'BACKOFFICESESSID',
        ],
        'request' => [
            'cookieValidationKey' => md5('babos'),
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'app\models\User',
            'loginUrl' => '/backoffice/user/login',
            'enableAutoLogin' => true,
        ],
    ],
];

if (file_exists(__DIR__ . '/local/backoffice.php')) {
    $backoffice_params = array_merge(
        $backoffice_params,
        require(__DIR__ . '/local/backoffice.php')
    );
}

return $backoffice_params;
