<?php

$components = [
    'assetManager' => [
        'linkAssets' => false,
        'assetMap'   => [],
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'pgsql:dbname=dbname;host=172.17.0.2',
        'username' => 'www',
        'password' => 'password',
        'charset' => 'utf8',
    ],
    'rabbit' => [
        'class' => \app\components\RabbitQueue::class,
        'user' => 'admin',
        'password' => 'admin',
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'transport' => [
            'streamOptions' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ],
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.yandex.com',
            'port' => 465,
            'encryption' => 'ssl',
            'username'      => 'noreply_test@mail.ru',
            'password'      => 'test_pass',
        ],
    ],
];

if (file_exists(__DIR__ . '/local/components.php')) {
    $components = array_merge(
        $components,
        require(__DIR__ . '/local/components.php')
    );
}

return $components;
