<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests/codeception');

$params = require(__DIR__ . '/params.php');
$components = require(__DIR__ . '/components.php');
$components = array_merge($components,[
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            #default
            '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
        ],
        'baseUrl' => '',
    ],
]);

$console_params = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => $components,
    'params' => $params,
];

if (file_exists(__DIR__ . '/components.php')) {
    $componentsLocal = require(__DIR__ . '/components.php');
    foreach ($componentsLocal as $key => $value) {
        $web_params['components'][$key] = $value;
    }
}

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $console_params['bootstrap'][] = 'gii';
    $console_params['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

if (file_exists(__DIR__ . '/local/console.php')) {
    $console_params = array_merge(
        $console_params,
        require(__DIR__ . '/local/console.php')
    );
}

require(__DIR__ . '/../config/dependencies.php');

return $console_params;
