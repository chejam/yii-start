<?php

$container = \Yii::$container;


$container->set(
    \yii\rbac\CheckAccessInterface::class,
    \app\components\AccessChecker::class
);

#region users
$container->set(
    \app\components\managers\interfaces\AuthManagerInterface::class,
    \app\components\managers\AuthManager::class
);

$container->set(
    \app\components\managers\interfaces\UserManagerInterface::class,
    \app\components\managers\UserManager::class
);

$container->set(
    \app\components\managers\interfaces\UserGridViewSettingsManagerInterface::class,
    \app\components\managers\UserGridViewSettingsManager::class
);
#endregion

#region email
$container->set(
    \app\components\managers\interfaces\EmailManagerInterface::class,
    \app\components\managers\EmailManager::class
);
#endregion