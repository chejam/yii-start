<?php

$params = [
    'cookieValidationKey' => md5('babos'),
    'auth' => [
        'hosts' => [
            'backoffice' => 'http://localhost:10001',
        ],
        'ttl' => 86400 // 1 день
    ],
    'googleApiKey' => '',
    'image' => [
        'path' => '/tmp/images/',
        'url' => 'http://localhost:10070/',
    ],
  /*  'email' => [
        'from' => 'noreply_test@bablocloud.ru',
        'createdOrder' => 'it@bablocloud.ru',
    ],*/
    'email' => [
        'from' => 'noreply_test@click2mice.com',
        'createdOrder' => 'it@click2mice.com',
    ],
    'uploadFile' => [
        'path' => '/tmp/',
    ],
];

if (file_exists(__DIR__ . '/local/params.php')) {
    $params = array_merge(
        $params,
        require(__DIR__ . '/local/params.php')
    );
}

return $params;
