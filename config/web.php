<?php

$params = require(__DIR__ . '/params.php');
$components = require(__DIR__ . '/components.php');
$components = array_merge($components, [
    'assetManager' => [
        'linkAssets' => true,
        'assetMap' => [],
    ],
    'formatter' => [
        'defaultTimeZone' => 'UTC',
        'timeZone' => 'Europe/Moscow',
        'dateFormat' => 'long',
        'datetimeFormat' => 'long',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            #default
            '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
        ]
    ],
]);

$web_params = [
    'id' => 'tmc-solutions',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'layout' => false,
    'components' => $components,
    'params' => $params,
];

if (YII_ENV_DEV) {
    $web_params['bootstrap'][] = 'debug';
    $web_params['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
}

if (file_exists(__DIR__ . '/local/web.php')) {
    $web_params =  yii\helpers\ArrayHelper::merge(
        $web_params,
        require(__DIR__ . '/local/web.php')
    );
}

return $web_params;
