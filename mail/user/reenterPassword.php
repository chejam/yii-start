<?php
/**
 * @var \app\models\PasswordResetCode $resetCode
 */

use yii\helpers\Url;
use app\models\User;

$user = $resetCode->user;
?>
<h3>Здравствуйте!</h3>
Ваш старый пароль был сброшен.
<h4>Для установки нового пароля перейдите по ссылке:</h4>
<a href="<?= \Yii::$app->params['auth']['hosts'][User::TYPE_BACKOFFICE] . Url::to(['/backoffice/user/recovery-password', 'hash' => $resetCode->token]) ?>" target="_blank">Изменить пароль</a>