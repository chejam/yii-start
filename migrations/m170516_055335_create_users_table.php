<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170516_055335_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id'                   => $this->primaryKey(),
            'username'             => $this->string(),
            'email'                => $this->string(),
            'surname'              => $this->string(),
            'name'                 => $this->string(),
            'patronymic'           => $this->string(),
            'phone1'               => $this->string(24),
            'phone2'               => $this->string(24),
            'phone2_ext'           => $this->string(12),
            'auth_key'             => $this->string(32),
            'password_hash'        => $this->string(),
            'password_reset_token' => $this->string(),
            'status'               => $this->string(24),
            'create_time'          => $this->integer()->unsigned(),
            'update_time'          => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
