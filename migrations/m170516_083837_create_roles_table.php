<?php

use yii\db\Migration;

/**
 * Handles the creation of table `roles`.
 */
class m170516_083837_create_roles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('roles', [
            'id'          => $this->primaryKey(),
            'entity_type' => $this->string(),
            'entity_id'   => $this->integer(),
            'name'        => $this->string(),
            'create_time' => $this->integer()->unsigned(),
            'update_time' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('roles');
    }
}
