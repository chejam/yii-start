<?php

use yii\db\Migration;

/**
 * Handles the creation of table `permissions`.
 */
class m170516_083848_create_permissions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('permissions', [
            'id'          => $this->primaryKey(),
            'entity_type' => $this->string(),
            'name'        => $this->string(),
            'label'       => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('permissions');
    }
}
