<?php

use yii\db\Migration;

/**
 * Handles the creation of table `roles_permissions`.
 */
class m170516_083904_create_roles_permissions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('roles_permissions', [
            'role_id'       => $this->integer(),
            'permission_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('roles_permissions_pk', 'roles_permissions', ['role_id', 'permission_id']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('roles_permissions');
    }
}
