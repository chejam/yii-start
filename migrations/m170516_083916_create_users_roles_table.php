<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_roles`.
 */
class m170516_083916_create_users_roles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_roles', [
            'id'          => $this->primaryKey(),
            'user_id'     => $this->integer(),
            'role_id'     => $this->integer(),
            'entity_type' => $this->string(),
            'entity_id'   => $this->integer(),
            'create_time' => $this->integer()->unsigned(),
            'update_time' => $this->integer()->unsigned(),
        ]);
        $this->addForeignKey('roles_users_roles_foreign_key', 'users_roles', 'role_id', 'roles', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('roles_users_roles_foreign_key', 'users_roles');
        $this->dropTable('users_roles');
    }
}
