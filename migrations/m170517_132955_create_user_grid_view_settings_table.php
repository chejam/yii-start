<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_grid_view_settings`.
 */
class m170517_132955_create_user_grid_view_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_grid_view_settings', [
            'id'      => $this->primaryKey(),
            'user_id' => $this->integer(),
            'route'   => $this->string(),
            'grid_id' => $this->string(),
            'params'  => 'jsonb',
        ]);

        $this->createIndex('route_grid_id_idx', 'user_grid_view_settings', ['user_id', 'route', 'grid_id']);
        $this->addForeignKey('user_grid_view_settings_users_foreign_key', 'user_grid_view_settings', 'user_id', 'users', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('user_grid_view_settings_users_foreign_key', 'user_grid_view_settings');
        $this->dropTable('user_grid_view_settings');
    }
}
