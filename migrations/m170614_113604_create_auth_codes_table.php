<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_codes`.
 */
class m170614_113604_create_auth_codes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('auth_codes', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'auth_code' => $this->string(),
            'original_user_id' => $this->integer(),
            'ttl' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auth_codes');
    }
}
