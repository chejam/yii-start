<?php

use yii\db\Migration;

/**
 * Handles the creation of table `password_reset_codes`.
 */
class m170614_114551_create_password_reset_codes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('password_reset_codes', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'token' => $this->string(32),
            'ttl' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('password_reset_codes');
    }
}
