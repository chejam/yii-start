<?php

namespace app\models;

use app\models\queries\AuthCodeQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "auth_codes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $auth_code
 * @property integer $original_user_id
 * @property integer $ttl
 */
class AuthCode extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'auth_codes';
    }

    /**
     * @return AuthCodeQuery
     */
    public static function find() : AuthCodeQuery
    {
        return new AuthCodeQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['user_id', 'original_user_id', 'auth_code'], 'required'],
            [['user_id', 'original_user_id', 'ttl'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['auth_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя для логина',
            'auth_code' => 'Токен проверки',
            'original_user_id' => 'ID пользователя для которого сгенерирован токен',
            'ttl' => 'TTL',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
