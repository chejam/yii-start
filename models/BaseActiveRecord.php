<?php

namespace app\models;

use yii\db\ActiveQuery;

abstract class BaseActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'createRelatedRecords']);
    }

    /**
     * @return bool
     */
    public function createRelatedRecords() : bool
    {
        return true;
    }

    /**
     * @param ActiveQuery $query
     * @param string $attribute
     * @param bool $partialMatch
     */
    protected function addCondition(ActiveQuery $query, string $attribute, bool $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }

        $attribute = static::tableName() . '.' . $attribute;

        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }

}