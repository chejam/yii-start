<?php

namespace app\models;
use app\models\queries\PasswordResetCodeQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "password_reset_codes".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $token
 * @property integer $ttl
 * @property User $user
 */
class PasswordResetCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'password_reset_codes';
    }

    /**
     * @return PasswordResetCodeQuery
     */
    public static function find() : PasswordResetCodeQuery
    {
        return new PasswordResetCodeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ttl', 'token'], 'required'],
            [['user_id', 'ttl'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['token'], 'string', 'max' => 32],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser() : ActiveQuery
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
