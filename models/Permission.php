<?php

namespace app\models;

use app\models\queries\PermissionQuery;
use yii\db\ActiveQuery;

/**
 * @property integer $id
 * @property string $entity_type
 * @property string $name
 * @property string $label
 * @property Role[] $role
 */
class Permission extends BaseActiveRecord
{
    const BACKOFFICE_LOGIN = 'backoffice.login';

    const BACKOFFICE_USER_VIEW = 'backoffice.user.view';
    const BACKOFFICE_USER_CREATE = 'backoffice.user.create';
    const BACKOFFICE_USER_UPDATE = 'backoffice.user.update';
    const BACKOFFICE_USER_DELETE = 'backoffice.user.delete';

    const BACKOFFICE_ROLE_VIEW = 'backoffice.role.view';
    const BACKOFFICE_ROLE_WRITE = 'backoffice.role.write';

    public static function permissionsByType()
    {
        return [
            Role::ROLE_TYPE_BACKOFFICE => [
                self::BACKOFFICE_LOGIN      => 'Авторизация в бекофисе',
                
                self::BACKOFFICE_USER_VIEW  => 'Просмотр пользователей',
                self::BACKOFFICE_USER_CREATE => 'Создание пользователей',
                self::BACKOFFICE_USER_UPDATE => 'Изменение пользователей',
                self::BACKOFFICE_USER_DELETE => 'Удаление пользователей',

                self::BACKOFFICE_ROLE_VIEW  => 'Просмотр ролей',
                self::BACKOFFICE_ROLE_WRITE => 'Изменения ролей',
            ],
            Role::ROLE_TYPE_USER => [
                self::BACKOFFICE_LOGIN      => 'Авторизация в бекофисе',
            ]
        ];
    }

    /**
     * @return PermissionQuery
     */
    public static function find() : PermissionQuery
    {
        return new PermissionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['entity_type', 'name', 'label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'entity_type' => 'Сущность',
            'name'        => 'Название',
            'label'       => 'Алиас',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRoles() : ActiveQuery
    {
        return $this->hasMany(Role::className(), ['id' => 'role_id'])
            ->viaTable('roles_permissions', ['permission_id' => 'id']);
    }
}
