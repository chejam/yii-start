<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $entity_type
 * @property integer $entity_id
 * @property string $name
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property Permission[] $permissions
 */
class Role extends BaseActiveRecord
{
    const ROLE_TYPE_BACKOFFICE = 'backoffice';

    const ROLE_TYPE_USER = 'user';
    
    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['name'], 'required'],
            [['entity_id'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['entity_type', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id'          => 'ID',
            'entity_type' => 'Сущность',
            'entity_id'   => 'Id сущностей',
            'name'        => 'Наименование роли',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPermissions() : ActiveQuery
    {
        return $this->hasMany(Permission::className(), ['id' => 'permission_id'])
            ->viaTable('roles_permissions', ['role_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
        ];
    }
}
