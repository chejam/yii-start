<?php

namespace app\models;

use app\components\behaviors\StringModifyBehavior;
use app\models\queries\UserQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\web\IdentityInterface;

/**
 * @property integer $id
 * @property string $username
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $email
 * @property string $phone1
 * @property string $phone2
 * @property string $phone2_ext
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $status
 * @property integer $create_time
 * @property integer $update_time
 *
 * @property UserRole[] $usersRoles
 */
class User extends BaseActiveRecord implements IdentityInterface
{
    const STATUS_NOT_ACTIVATED = 'not_activated';
    const STATUS_ACTIVATED = 'activated';
    const STATUS_LOCKED = 'locked';

    const TYPE_BACKOFFICE = 'backoffice';

    const SCENARIO_DEFAULT = self::SCENARIO_USER;
    const SCENARIO_USER = 'user';
    const SCENARIO_REGISTER_USER = 'register_user';

    private $_permissions = null;

    public function scenarios()
    {
        return [
            self::SCENARIO_USER => ['status', 'username', 'surname', 'name', 'patronymic', 'email', 'password_hash', 'phone1', 'phone2', 'phone2_ext'],
            self::SCENARIO_REGISTER_USER => ['email'],
        ];
    }

    public static function getStatuses() : array
    {
        return [
            self::STATUS_ACTIVATED     => 'Активирован',
            self::STATUS_LOCKED        => 'Заблокирован',
        ];
    }

    public function getStatusLabel() : string
    {
        $statuses = self::getStatuses();
        return $statuses[$this->status];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'users';
    }

    /**
     * @return UserQuery
     */
    public static function find() : UserQuery
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['status'], 'default', 'value' => self::STATUS_ACTIVATED],
            [['status', 'username', 'surname', 'name', 'patronymic', 'email', 'password_hash'], 'required'],
            [['phone1', 'phone2', 'status'], 'string', 'max' => 24],
            [['phone2_ext'], 'string', 'max' => 12],
            [['status'], 'in', 'range' => array_keys(self::getStatuses())],
            [['username', 'surname', 'name', 'patronymic', 'email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['email'], 'unique', 'message' => 'Данный E-mail уже используется'],
            [['email'], 'email'],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id'                   => 'ID',
            'roleName'             => 'Роль',
            'typeName'             => 'Тип',
            'username'             => 'Имя пользователя',
            'surname'              => 'Фамилия',
            'name'                 => 'Имя',
            'patronymic'           => 'Отчество',
            'phone1'               => 'Мобильный телефон',
            'phone2'               => 'Рабочий телефон',
            'phone2_ext'           => 'Рабочий телефон, доп.',
            'email'                => 'E-mail',
            'auth_key'             => 'Ключ авторизации',
            'password_hash'        => 'Хэш пароля',
            'password_reset_token' => 'Токен для восстановления пароля',
            'status'               => 'Статус',
            'create_time'          => 'Зарегистрирован',
            'update_time'          => 'Дата последнего изменения',

            'spaceName'   => 'Название площадки',
            'cityName'    => 'Город',
            'companyName' => 'Название Компании',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
            [
                'class'        => StringModifyBehavior::className(),
                'stringFields' => ['email'],
            ],
        ];
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public static function findIdentity($id) : User
    {
        return User::findOne($id);
    }

    /**
     * @param mixed $token
     * @param string|null $type
     * @return User
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::find()->byAccessToken($token)->one();
    }

    /**
     * @return string
     */
    public function getAuthKey() : string
    {
        return $this->auth_key;
    }

    /**
     * @return string
     */
    public function generateAuthKey() : string
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey) : bool
    {
        return $this->auth_key === $authKey;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $password) : bool
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password) : User
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
        return $this;
    }

    public function beforeDelete()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        if (parent::beforeDelete()) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollBack();
            return false;
        }
    }

    public function can($permissions)
    {
        if (!is_array($permissions)) {
            $permissions = [$permissions];
        }
        if (empty($this->_permissions)) {
            $this->_permissions = $this->role->getPermissions()->indexBy('name')->all();
        }
        foreach ($permissions as $permission) {
            if (isset($this->_permissions[$permission])) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersRoles() : ActiveQuery
    {
        return $this->hasMany(UserRole::class, ['user_id' => 'id']);
    }
}
