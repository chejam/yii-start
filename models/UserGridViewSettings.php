<?php

namespace app\models;

use app\models\queries\UserGridViewSettingsQuery;
use dstotijn\yii2jsv\JsonSchemaValidator;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_grid_view_settings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $route
 * @property string $grid_id
 * @property string $params
 */
class UserGridViewSettings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'user_grid_view_settings';
    }

    /**
     * @return UserGridViewSettingsQuery
     */
    public static function find() : UserGridViewSettingsQuery
    {
        return new UserGridViewSettingsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['route', 'user_id', 'grid_id', 'params'], 'required'],
            [['user_id'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['params'], 'string'],
            [['params'], JsonSchemaValidator::class,
                'schema' => 'file://' . Yii::getAlias('@app/schemas/json/gridViewParamsSchema.json')],
            [['route', 'grid_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id'      => 'ID',
            'user_id' => 'ID пользователя',
            'route'   => 'Путь к странице с GridView',
            'grid_id' => 'ID GridView',
            'params'  => 'Настройки отображения колонок',
        ];
    }
}
