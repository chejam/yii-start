<?php

namespace app\models;

use app\models\queries\PermissionQuery;
use app\models\queries\UserRoleQuery;
use yii\db\ActiveQuery;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $role_id
 * @property string $entity_type
 * @property integer $entity_id
 *
 * @property Role $role
 */
class UserRole extends BaseActiveRecord
{

    /**
     * @return UserRoleQuery
     */
    public static function find() : UserRoleQuery
    {
        return new UserRoleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['entity_type'], 'string', 'max' => 255],
            [['entity_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }
}
