<?php

namespace app\models\backoffice\forms;

use app\models\forms\BaseLoginForm;
use app\models\User;
use app\models\Role;

class LoginForm extends BaseLoginForm
{
    protected $userType = User::TYPE_BACKOFFICE;

    /**
     * @param string $login
     * @return User
     */
    protected function findUser(string $login)
    {
        $user = User::find()->byType([
            Role::ROLE_TYPE_BACKOFFICE,
            Role::ROLE_TYPE_USER,
        ])->byEmail($login)->one();
        return $user;
    }
}