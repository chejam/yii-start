<?php

namespace app\models\backoffice\forms;

use app\components\managers\interfaces\AuthManagerInterface;
use app\components\managers\interfaces\EmailManagerInterface;
use app\models\PasswordResetCode;
use app\models\User;
use yii\base\Model;

class RecoveryForm extends Model
{
    public $email;

    /**
     * @return array
     */
    public function attributeLabels() : array
    {
        return [
            'email' => 'Логин (e-mail):',
        ];
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * @return bool|PasswordResetCode
     */
    public function createToken() : PasswordResetCode
    {
        if ($this->validate()) {
            $user = User::find()->byEmail($this->email)->one();
            if ($user === null) {
                $this->addError('user', 'Пользователь с таким email не найден');
            } else {
                /** @var AuthManagerInterface $authManager */
                $authManager = \Yii::$container->get(AuthManagerInterface::class);
                $token = $authManager->generateResetPasswordToken($user->id);
                if ($token->isNewRecord) {
                    $this->addError('email', 'Не удалось сгенерировать код восстановления');
                }
                if (!$this->hasErrors()) {
                    /** @var EmailManagerInterface $emailManager */
                    $emailManager = \Yii::$container->get(EmailManagerInterface::class);
                    $emailManager->sendMessageInstant(
                        $user->email,
                        'Восстановление пароля',
                        'user/reenterPassword',
                        ['resetCode' => $token]
                    );
                    return $token;
                }
            }
        }
        return !$this->hasErrors();
    }
}