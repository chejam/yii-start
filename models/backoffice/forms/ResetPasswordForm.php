<?php

namespace app\models\backoffice\forms;

use app\models\User;
use yii\base\Model;

class ResetPasswordForm extends Model
{
    /** @var string */
    public $password;
    /** @var string */
    public $oldPassword;
    /** @var string */
    public $newPassword;

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['password', 'newPassword', 'oldPassword'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'    => 'Новый пароль',
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Повторите новый пароль',
        ];
    }

    /**
     * @return bool
     */
    public function resetPassword() : bool
    {
        if (!$this->validate()) {
            return false;
        }
        /** @var User $user */
        $user = \Yii::$app->user->identity;
        if ($this->newPassword == $this->password) {
            if ($user->validatePassword($this->oldPassword)) {
                $user->setPassword($this->password);
                $success = $user->save();
            } else {
                $this->addError('oldPassword', 'Введенный пароль неверен');
                $success = false;
            }
        } else {
            $this->addError('newPassword', 'Пароли должны совпадать');
            $success = false;
        }

        return $success;
    }
}