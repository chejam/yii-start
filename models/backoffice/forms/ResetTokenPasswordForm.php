<?php

namespace app\models\backoffice\forms;

use app\models\PasswordResetCode;
use yii\base\Model;

class ResetTokenPasswordForm extends Model
{
    /** @var string */
    public $password;
    /** @var string */
    public $passwordConfirmation;

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['password', 'passwordConfirmation'], 'required'],
            [['passwordConfirmation'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'passwordConfirmation' => 'Подтвердите новый пароль',
        ];
    }

    /**
     * @param PasswordResetCode $token
     * @return bool
     */
    public function save(PasswordResetCode $token) : bool
    {
        if ($this->validate()) {
            return \Yii::$app->db->transaction(function () use ($token) {
                $token->user->setPassword($this->password);
                $result = $token->user->save();
                $result &= $token->delete();
                return $result;
            });
        }
        return false;
    }
}