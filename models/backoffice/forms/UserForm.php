<?php

namespace app\models\backoffice\forms;

use app\models\Role;
use app\models\User;
use app\models\UserRole;
use yii\base\Model;
use app\components\managers\interfaces\EmailManagerInterface;

class UserForm extends Model
{
    const SCENARIO_DEFAULT = self::SCENARIO_CREATE;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_REGISTER_USER = 'register_user';

    public $id;
    public $username;
    public $surname;
    public $name;
    public $patronymic;
    public $phone1;
    public $phone2;
    public $phone2_ext;
    public $email;
    public $role_id;
    public $status;
    public $password;
    public $passwordConfirmation;

    public $related_entity_id;

    /** @var null|User */
    public $model = null;

    public function scenarios()
    {
        return [
            self::SCENARIO_UPDATE        => ['id', 'username', 'surname', 'name', 'patronymic', 'phone1', 'phone2', 'phone2_ext', 'email', 'password', 'passwordConfirmation', 'role_id', 'status'],
            self::SCENARIO_CREATE        => ['username', 'surname', 'name', 'patronymic', 'phone1', 'phone2', 'phone2_ext', 'email', 'password', 'passwordConfirmation', 'role_id'],
            self::SCENARIO_REGISTER_USER => ['email', 'password', 'passwordConfirmation'],
        ];
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['email'], 'trim'],
            [['email', 'surname', 'name', 'patronymic', 'phone1', 'phone2', 'phone2_ext', 'role_id', 'related_entity_id', 'status', 'password', 'passwordConfirmation'], 'required'],
            [['role_id', 'related_entity_id'], 'integer', 'max' => 2147483647, 'min' => -2147483648],
            [['email'], 'email'],
            [['passwordConfirmation'], 'compare', 'compareAttribute' => 'password'],
            [['password', 'passwordConfirmation', 'email', 'username', 'surname', 'name', 'patronymic', 'status'], 'string', 'max' => 255],
            [['phone1', 'phone2'], 'string', 'max' => 24],
            [['phone2_ext'], 'string', 'max' => 12],
            [['phone1', 'phone2'], 'match', 'pattern' => '/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){6,14}(\s*)?$/', 'message' => 'Поле «{attribute}» должно содержать числовое значение.'],
            [['fax'], 'match', 'pattern' => '/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){6,14}(\s*)?$/', 'message' => 'Поле «{attribute}» должно содержать числовое значение.'],
            [['phone2_ext'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'role_id'              => 'Роль',
            'status'               => 'Статус',
            'username'             => 'Имя пользователя',
            'surname'              => 'Фамилия',
            'name'                 => 'Имя',
            'patronymic'           => 'Отчество',
            'phone1'               => 'Мобильный телефон',
            'phone2'               => 'Рабочий телефон',
            'phone2_ext'           => 'доб.',
            'email'                => 'E-mail пользователя',
            'password'             => 'Пароль',
            'passwordConfirmation' => 'Подтверждение пароля',
            'related_entity_id'    => 'Связана с',
        ];
    }

    /**
     * @param User $user
     * @return UserForm
     */
    public static function loadFromUser(User $user, $scenario = null) : UserForm
    {
        $scenario = $scenario ?? ($user->getIsNewRecord() ? self::SCENARIO_CREATE : self::SCENARIO_UPDATE);
        $userForm = new UserForm(['scenario' => $scenario]);
        $userForm->setAttributes($user->getAttributes());
        $userForm->model = $user;
        $userRole = UserRole::find()->where(['user_id' => $user->id])->one();
        if ($userRole) {
            $userForm->role_id = $userRole->role_id;
        }
        return $userForm;
    }

    /**
     * @return bool
     */
    public function save() : bool
    {
        $result = $this->validate();
        $user = $this->model;

        if ($result) {
            $userIsNewRecord = $user->isNewRecord;
            if ($userIsNewRecord) {
                $password = \Yii::$app->security->generateRandomString(6);
                $user->setPassword($password);
            }
            $result = \Yii::$app->db->transaction(function () use ($user) {
                $user->setAttributes($this->getAttributes());
                if ($this->password) {
                    $user->setPassword($this->password);
                }
                if (in_array(
                    $this->scenario,
                    [self::SCENARIO_CREATE, self::SCENARIO_REGISTER_USER])
                ) {
                    $user->generateAuthKey();
                    $user->status = User::STATUS_ACTIVATED;
                }
                if ($this->scenario == self::SCENARIO_REGISTER_USER) {
                    $transactionResult = $user->save();
                    $rolesResult = true;
                    /** @var Role $roleUser */
                    $roleUser = Role::find()->where(['entity_type' => Role::ROLE_TYPE_USER])->one();
                    $rolesResult &= $this->addUserRole($user->id, $roleUser);
                } else {
                    $userFullName = $user->surname . ' ' . mb_substr($user->name, 0, 1) . '.' . mb_substr($user->patronymic, 0, 1) . '.';
                    $user->username = $userFullName;
                    $transactionResult = $user->save();
                    $userOldRoles = UserRole::find()->where(['user_id' => $user->id])->all();
                    $rolesResult = true;
                    $roleExists = false;
                    foreach ($userOldRoles as $userOldRole) {
                        if ($userOldRole->role_id != $this->role_id) {
                            $rolesResult &= $userOldRole->delete();
                        } else {
                            $roleExists = true;
                        }
                    }
                    if (!$roleExists) {
                        $userNewRole = new UserRole(['user_id' => $user->id, 'role_id' => $this->role_id, 'entity_type' => Role::ROLE_TYPE_BACKOFFICE]);
                        $rolesResult &= $userNewRole->save();
                    }
                }
                return $rolesResult && $transactionResult;
            });
            if ($result && !empty($user) && $userIsNewRecord) {
                $user->auth_key = $user->generateAuthKey();
                /** @var EmailManagerInterface $emailManager */
                $emailManager = \Yii::$container->get(EmailManagerInterface::class);
                $emailManager->sendMessageInstant($user->email, 'Регистрация', 'user/createdNewUser', [
                    'user'       => $user,
                    'password'   => $this->password ?? $password ?? '',
                ]);
            }
            if (!$result) {
                $this->addErrors($user->getErrors());
            } else {
                $this->id = $user->id;
            }
        }
        return $result;
    }

    public function addUserRole($user_id, Role $role, $entity_id = null)
    {
        $findUserRole = UserRole::find()->where([
            'user_id'     => $user_id,
            'role_id'     => $role->id,
            'entity_type' => $role->entity_type,
        ]);
        if (!empty($entity_id)) {
            $findUserRole->andWhere([
                'entity_id' => $entity_id,
            ]);
        }
        $needNewRole = !$findUserRole->exists();
        if ($needNewRole) {
            $newUserRole = new UserRole([
                'user_id'     => $user_id,
                'role_id'     => $role->id,
                'entity_type' => $role->entity_type,
                'entity_id'   => $entity_id,
            ]);
            return $newUserRole->save();
        }
        return true;
    }
}