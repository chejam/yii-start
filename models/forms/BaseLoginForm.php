<?php

namespace app\models\forms;

use app\models\User;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class BaseLoginForm extends Model
{
    protected $userType;

    /** @var string */
    public $login;
    /** @var string */
    public $password;
    /** @var bool */
    public $rememberMe;
    /** @var int */
    public $rememberTime = 2592000; // 3600 * 24 * 30

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (empty($this->userType)) {
            throw new InvalidConfigException('userType property must be set');
        }
    }

    /**
     * @return bool
     */
    protected function checkAccess(User $user) : bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['rememberMe'], 'default', 'value' => false],
            [['login', 'password'], 'required'],
            [['rememberMe'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login'      => 'Почта',
            'password'   => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     * @return bool
     */
    public function login() : bool
    {
        if (!$this->validate()) {
            return false;
        }
        $user = $this->findUser($this->login);
        if (isset($user) && $user->status == User::STATUS_LOCKED) {
            $this->addError('login', 'Данный пользователь заблокирован, обратитесь к Администратору');
        }
        if ($user === null) {
            $this->addError('login', 'Пользователя с такими почтой/именем не существует');
        } else {
            if (!$user->validatePassword($this->password)) {
                $this->addError('password', 'Введенный пароль неверен');
            }
            $this->checkAccess($user);
        }

        if ($this->hasErrors()) {
            return false;
        } else {
            return \Yii::$app->user->login($user, $this->rememberMe ? $this->rememberTime : 0);
        }
    }

    /**
     * @param string $login
     * @return User
     */
    protected function findUser(string $login)
    {
        $user = User::find()->byType($this->userType)->byEmail($login)->one();
        return $user;
    }
}