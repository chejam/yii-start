<?php

namespace app\models\queries;

use yii\db\ActiveQuery;

class AuthCodeQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return AuthCodeQuery
     */
    public function byOriginalUser(int $id) : AuthCodeQuery
    {
        return $this->andWhere(['original_user_id' => $id]);
    }

    /**
     * @param int|null $time
     * @return AuthCodeQuery
     */
    public function checkTtl($time = null) : AuthCodeQuery
    {
        if ($time === null) {
            $time = time();
        }
        return $this->andWhere('ttl > :time', [':time' => $time]);
    }

    /**
     * @param string $authCode
     * @return AuthCodeQuery
     */
    public function byAuthCode(string $authCode) :AuthCodeQuery
    {
        return $this->andWhere(['auth_code' => $authCode]);
    }
}