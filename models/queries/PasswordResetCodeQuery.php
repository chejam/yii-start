<?php

namespace app\models\queries;

use yii\db\ActiveQuery;

class PasswordResetCodeQuery extends ActiveQuery
{
    /**
     * @param int $id
     * @return PasswordResetCodeQuery
     */
    public function byUser(int $id) : PasswordResetCodeQuery
    {
        return $this->andWhere(['user_id' => $id]);
    }

    /**
     * @param int|null $time
     * @return PasswordResetCodeQuery
     */
    public function checkTtl($time = null) : PasswordResetCodeQuery
    {
        if ($time === null) {
            $time = time();
        }
        return $this->andWhere('ttl > :time', [':time' => $time]);
    }

    /**
     * @param string $token
     * @return PasswordResetCodeQuery
     */
    public function byToken(string $token) :PasswordResetCodeQuery
    {
        return $this->andWhere(['token' => $token]);
    }
}