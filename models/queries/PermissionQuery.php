<?php

namespace app\models\queries;

use app\models\Permission;
use yii\db\ActiveQuery;

/**
 * Class PermissionQuery
 *
 * @method Permission[] all($db = null)
 */
class PermissionQuery extends ActiveQuery
{
    /**
     * @param string $name
     * @return PermissionQuery
     */
    public function byName(string $name) : PermissionQuery
    {
        return $this->andWhere(['name' => $name]);
    }

    /**
     * @param string $entityType
     * @return PermissionQuery
     */
    public function byType(string $entityType) : PermissionQuery
    {
        return $this->andWhere(['entity_type' => $entityType]);
    }
}