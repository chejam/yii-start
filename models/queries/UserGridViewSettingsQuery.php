<?php

namespace app\models\queries;

use yii\db\ActiveQuery;

class UserGridViewSettingsQuery extends ActiveQuery
{
    /**
     * @param string $route
     * @return UserGridViewSettingsQuery
     */
    public function byRoute(string $route) : UserGridViewSettingsQuery
    {
        return $this->andWhere(['route' => $route]);
    }

    /**
     * @param string $gridId
     * @return UserGridViewSettingsQuery
     */
    public function byGridId(string $gridId)
    {
        return $this->andWhere(['grid_id' => $gridId]);
    }

    /**
     * @param int $userId
     * @return UserGridViewSettingsQuery
     */
    public function byUserId(int $userId) : UserGridViewSettingsQuery
    {
        return $this->andWhere(['user_id' => $userId]);
    }
}