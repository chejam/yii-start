<?php

namespace app\models\queries;

use app\models\User;
use app\models\UserRole;
use yii\db\ActiveQuery;

/**
 * Class UserQuery
 *
 * @method User one($db = null)
 * @method User[] all($db = null)
 */
class UserQuery extends ActiveQuery
{
    /**
     * @param string $username
     * @return UserQuery
     */
    public function byUsername(string $username) : UserQuery
    {
        return $this->andWhere(['username' => mb_convert_case($username, MB_CASE_LOWER , "UTF-8")]);
    }

    /**
     * @param string $email
     * @return UserQuery
     */
    public function byEmail(string $email) : UserQuery
    {
        return $this->andWhere(['email' =>  mb_convert_case($email, MB_CASE_LOWER , "UTF-8")]);
    }

    /**
     * @param string $authKey
     * @return UserQuery
     */
    public function byAuthKey(string $authKey) : UserQuery
    {
        return $this->andWhere(['auth_key' => $authKey]);
    }

    /**
     * @param string $accessToken
     * @return UserQuery
     */
    public function byAccessToken(string $accessToken) : UserQuery
    {
        return $this->andWhere(['access_token' => $accessToken]);
    }

    /**
     * @param bool $active
     * @return UserQuery
     */
    public function active(bool $active = true) : UserQuery
    {
        if ($active) {
            return $this->andWhere(['users.status' => User::STATUS_ACTIVATED]);
        } else {
            return $this->andWhere(['<>', 'users.status', User::STATUS_LOCKED]);
        }
    }

    /**
     * @param bool $exist
     * @return UserQuery
     */
    public function exist(bool $exist = true) : UserQuery
    {
        if ($exist) {
            return $this->andWhere(['<>', 'status', User::STATUS_LOCKED]);
        } else {
            return $this->andWhere(['status' => User::STATUS_LOCKED]);
        }
    }

    /**
     * @param string|array $entityType
     * @return UserQuery
     */
    public function byType($entityType) : UserQuery
    {
        return $this->joinWith('usersRoles', true, 'RIGHT JOIN')->andWhere([UserRole::tableName() . '.entity_type' => $entityType]);
    }
}