<?php

namespace app\models\queries;

use app\models\Permission;
use app\models\UserRole;
use yii\db\ActiveQuery;

/**
 * Class PermissionQuery
 *
 * @method UserRole[] all($db = null)
 */
class UserRoleQuery extends ActiveQuery
{

    public function byUserId($userId) : UserRoleQuery
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    public function findPermissions($userId)
    {
        $permissionsId = $this
            ->byUserId($userId)
            ->joinWith(['role', 'role.permissions'])
            ->select([Permission::tableName() . '.id'])
            ->column();
        return Permission::find()->where(['id' => $permissionsId])->indexBy('name')->all();
    }
}