<?php

namespace  app\models\queueMessages;

use app\components\RabbitQueue;
use yii\base\Model;

abstract class BaseQueueMessage extends Model
{
    /** @var int */
    public $ttl = 5;
    /** @var int */
    public $attempts = 0;

    /**
     * @return string
     */
    abstract static function getExchange() : string;

    /**
     * @return string
     */
    abstract static function getRoutingKey() : string;

    /**
     * @return string
     */
    abstract static function getQueue() : string;

    /**
     * @return bool
     */
    abstract protected function handle() : bool;

    public function process()
    {
        $this->attempts++;
        if (!$this->handle()) {
            if (!$this->isTtlReached()) {
                $this->push();
            } else {
                \Yii::error('ttl reached for message: ' . get_class($this) .  ". \n" . $this->exportToJson());
            }
        }
    }

    /**
     * @param string $data
     * @return BaseQueueMessage
     */
    public static function loadFromJson(string $data) : BaseQueueMessage
    {
        $model = new static;
        $model->setAttributes(json_decode($data, true));
        $model->attempts++;
        return $model;
    }

    /**
     * @return string
     */
    public function exportToJson() : string
    {
        return json_encode($this);
    }

    /**
     * @return bool
     */
    public function isTtlReached() : bool
    {
        return $this->attempts >= $this->ttl;
    }

    /**
     * @param array $properties
     * @return static
     */
    public function push(array $properties = []) : BaseQueueMessage
    {
        /** @var RabbitQueue $rabbitQueue */
        $rabbitQueue = \Yii::$app->rabbit;
        $rabbitQueue->push($this, $properties);
        return $this;
    }

}