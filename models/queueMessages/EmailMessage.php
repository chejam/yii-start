<?php

namespace app\models\queueMessages;

use app\components\managers\interfaces\EmailManagerInterface;
use app\models\User;
use app\models\order\Order;

class EmailMessage extends BaseQueueMessage
{
    const EXCHANGE = 'message';
    const ROUTING_KEY = 'message.email';
    const QUEUE = 'email';

    public $recipient;
    public $topic;
    public $view;
    public $message;
    public $params;

    /**
     * @return string
     */
    public static function getRoutingKey() : string
    {
        return self::ROUTING_KEY;
    }

    /**
     * @return string
     */
    public static function getExchange() : string
    {
        return self::EXCHANGE;
    }

    /**
     * @return string
     */
    public static function getQueue() : string
    {
        return self::QUEUE;
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        return array_merge(parent::rules(), [
            [['recipient', 'topic', 'view', 'message'], 'string'],
            [['params'], 'safe'],
        ]);
    }

    /**
     * @return bool
     */
    public function handle() : bool
    {
        /** @var EmailManagerInterface $emailManager */
        $emailManager = \Yii::$container->get(EmailManagerInterface::class);
        if (isset($this->view)) {
            if (isset($this->params['user']) && is_numeric($this->params['user'])) {
                $this->params['user'] = User::findOne($this->params['user']);
            }
            if (isset($this->params['order']) && is_numeric($this->params['order'])) {
                $this->params['order'] = Order::findOne($this->params['order']);
            }
            return $emailManager->sendMessageInstant($this->recipient, $this->topic, $this->view, $this->params);
        } else {
            return $emailManager->sendPlainTextMessageInstant($this->recipient, $this->topic, $this->message ?? "");
        }
    }
}