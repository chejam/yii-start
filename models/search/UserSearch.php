<?php

namespace app\models\search;

use app\models\queries\UserQuery;
use app\models\User;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public $typeName;
    public $roleName;
    public $spaceName;
    public $cityName;
    public $companyName;

    public function rules() : array
    {
        return [
            [[
                'typeName', 'email', 'username', 'roleName', 'status',
                'spaceName', 'cityName', 'companyName'
            ], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params) : ActiveDataProvider
    {
        $select = ['users.*'];
        $joinWith = ['usersRoles.role'];

        /** @var UserQuery $query */
        $query = User::find();
        if (!empty($params['full-search'])) {
            $query = $query->andWhere(
                [
                    'ilike',
                    "CONCAT_WS(' ', username, users.email, roles.name)",
                    $params['full-search']
                ]
            );
        }

        $query = $query->joinWith($joinWith)->select($select);



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                    'email',
                    'username',
                    'roleName' => [
                        'asc' => ['roles.name' => SORT_ASC],
                        'desc' => ['roles.name' => SORT_DESC],
                        'label' => 'Роль'
                    ],
                    'status',
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addCondition($query, 'email', true);
        $this->addCondition($query, 'username', true);
        $this->addCondition($query, 'roles.id');
        $this->addCondition($query, 'status');
        $query->andFilterWhere(['roles.id' => $this->roleName]);

        return $dataProvider;
    }
}