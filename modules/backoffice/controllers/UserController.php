<?php

namespace app\modules\backoffice\controllers;

use app\components\managers\interfaces\AuthManagerInterface;
use app\components\managers\interfaces\UserGridViewSettingsManagerInterface;
use app\components\managers\interfaces\UserManagerInterface;
use app\models\backoffice\forms\LoginForm;
use app\models\backoffice\forms\RecoveryForm;
use app\models\backoffice\forms\ResetTokenPasswordForm;
use app\models\backoffice\forms\UserForm;
use app\models\Permission;
use app\models\Role;
use app\models\search\UserSearch;
use Yii;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'recovery', 'recovery-password'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'grid-view-settings', 'profile'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['index', 'view', 'download'],
                        'allow'   => true,
                        'roles'   => [Permission::BACKOFFICE_USER_VIEW],
                    ],
                    [
                        'actions' => ['create'],
                        'allow'   => true,
                        'roles'   => [Permission::BACKOFFICE_USER_CREATE],
                    ],
                    [
                        'actions' => ['update'],
                        'allow'   => true,
                        'roles'   => [Permission::BACKOFFICE_USER_UPDATE],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow'   => true,
                        'roles'   => [Permission::BACKOFFICE_USER_DELETE],
                    ],
                    [
                        'actions' => ['register'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'           => ['POST'],
                    'logout'           => ['POST'],
                    'gridViewSettings' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $loginForm = new LoginForm();
        if ($loginForm->load(\Yii::$app->request->post()) && $loginForm->login()) {
            return $this->goBack();
        }

        $this->layout = 'main-login';
        return $this->render('login', [
            'loginForm' => $loginForm,
            'recoveryForm' => new RecoveryForm(),
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id) : User
    {
        if (($user = User::findOne($id)) !== null) {
            return $user;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionIndex() : string
    {
        $searchModel = new UserSearch();

        $roles = Role::find()->indexBy('id')->all();

        return $this->render('list', [
            'dataProvider' => $searchModel->search(Yii::$app->request->get()),
            'roles'        => $roles,
            'searchModel'  => $searchModel,
        ]);
    }

    public function actionDownload()
    {
        $searchModel = new UserSearch();

        $roles = Role::find()->indexBy('id')->all();

        return $this->renderPartial('_gridView', [
            'dataProvider' => $searchModel->search(Yii::$app->request->get()),
            'roles'        => $roles,
            'searchModel'  => $searchModel,
            'generate'     => true,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return string
     */
    public function actionView($id) : string
    {
        $user = $this->findModel($id);

        return $this->render('view', [
            'user' => $user,
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete(int $id) : Response
    {
        /** @var UserManagerInterface $userManager */
        $userManager = \Yii::$container->get(UserManagerInterface::class);
        $userManager->changeStatus($this->findModel($id), User::STATUS_LOCKED);
        return $this->redirect(['index']);
    }

    public function actionGridViewSettings()
    {
        /** @var UserGridViewSettingsManagerInterface $gridViewSettingsManager */
        $gridViewSettingsManager = \Yii::$container->get(UserGridViewSettingsManagerInterface::class);
        $route = \Yii::$app->request->post('route');
        $gridId = \Yii::$app->request->post('grid_id');
        $params = \Yii::$app->request->post('params');
        if ($route === null || $gridId === null || $params === null) {
            throw new HttpException(400);
        }
        $settings = $gridViewSettingsManager->setSettings(\Yii::$app->user->getId(), $route, $gridId, $params);
        if ($settings->hasErrors()) {
            throw new HttpException(400);
        }
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $newUser = new User();
        $userForm = UserForm::loadFromUser($newUser);
        if ($userForm->load(Yii::$app->request->post()) && $userForm->save()) {
            return $this->redirect(['view', 'id' => $userForm->id]);
        } else {
            $roles = Role::find()->where(['entity_type' => User::TYPE_BACKOFFICE])->indexBy('id')->all();
            return $this->render('create', [
                'user' => $userForm,
                'roles' => $roles,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionRegister()
    {
        $newUser = new User();
        $userForm = UserForm::loadFromUser($newUser, User::SCENARIO_REGISTER_USER);
        $newUser->scenario = User::SCENARIO_REGISTER_USER;
        $this->layout = 'main-login';

        if ($userForm->load(Yii::$app->request->post()) && $userForm->save()) {
            return $this->redirect(['login']);
        } else {
            return $this->render('createUser', [
                'user' => $userForm,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $userForm = UserForm::loadFromUser($user);

        if ($userForm->load(Yii::$app->request->post()) && $userForm->save($user)) {
            return $this->redirect(['view', 'id' => $userForm->id]);
        } else {
            $roles = Role::find()->where(['entity_type' => User::TYPE_BACKOFFICE])->indexBy('id')->all();
            return $this->render('update', [
                'user' => $userForm,
                'roles' => $roles,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionProfile()
    {
        $userId = \Yii::$app->user->id;
        $user = $this->findModel($userId);
        $userForm = UserForm::loadFromUser($user);

        if ($userForm->load(Yii::$app->request->post()) && $userForm->save()) {
            return $this->redirect(['/']);
        } else {
            $roles = Role::find()->where(['entity_type' => User::TYPE_BACKOFFICE])->indexBy('id')->all();
            return $this->render('update', [
                'user' => $userForm,
                'roles' => $roles,
            ]);
        }
    }

    public function actionRecovery()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $recoveryForm = new RecoveryForm();
        if ($recoveryForm->load(\Yii::$app->request->post()) && ($token = $recoveryForm->createToken())) {
            \Yii::$app->getSession()->setFlash('login-info', 'Письмо с ссылкой на восстановление пароля отправлено на ' . $recoveryForm->email . ' и действует до ' . date('d.m.Y H:i', $token->ttl));
            return $this->redirect(['login']);
        }

        $this->layout = 'main-login';
        return $this->render('login', [
            'loginForm' => new LoginForm(),
            'recoveryForm' => $recoveryForm,
        ]);
    }

    public function actionRecoveryPassword(string $hash)
    {
        /** @var AuthManagerInterface $authManager */
        $authManager = \Yii::$container->get(AuthManagerInterface::class);
        $passwordResetCode = $authManager->resetPassword($hash);
        if ($passwordResetCode === null) {
            \Yii::$app->getSession()->setFlash('login-error', 'Ссылка устарела');
        }
        $resetPasswordForm = new ResetTokenPasswordForm();
        $resetPasswordForm->load(\Yii::$app->request->post());
        if ($resetPasswordForm->load(\Yii::$app->request->post()) && $resetPasswordForm->save($passwordResetCode)) {
            return $this->redirect(['login']);
        }

        $this->layout = 'main-login';
        return $this->render('recovery', [
            'resetForm' => $resetPasswordForm,
        ]);
    }
}
