<?php
use \app\widgets\sideBarMenu\SideBarMenuAdminLteWidget;
use \app\models\Permission;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= SideBarMenuAdminLteWidget::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items'   => [
                    ['label' => 'Работа с пользователями', 'options' => ['class' => 'header'], 'permission' => Permission::BACKOFFICE_USER_VIEW],
                    [
                        'label'      => 'Пользователи',
                        'icon'       => 'fa fa-users',
                        'permission' => Permission::BACKOFFICE_USER_VIEW,
                        'items' => [
                            ['label' => 'Список пользователей', 'icon' => 'fa fa-users', 'url' => ['user/index'], 'permission' => Permission::BACKOFFICE_USER_VIEW],
                            ['label' => 'Добавить пользователя', 'icon' => 'fa fa-user-plus', 'url' => ['user/create'], 'permission' => Permission::BACKOFFICE_USER_CREATE],
                        ]
                    ]
                ],
            ]
        ) ?>

    </section>

</aside>
