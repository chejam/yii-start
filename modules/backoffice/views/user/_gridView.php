<?php
/**
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel User
 * @var $roles \app\models\Role[]
 * @var $generate bool
 */


use app\models\Permission;
use app\models\User;
use app\widgets\gridView\GridViewWidget;
use yii\helpers\ArrayHelper;

echo GridViewWidget::widget([
    'dataProvider'        => $dataProvider,
    'id'                  => 'users_list',
    'filterModel'         => $searchModel,
    'renderButtons'       => [
        GridViewWidget::RENDER_SETTINGS_BUTTON,
        GridViewWidget::RENDER_DOWNLOAD_BUTTON,
        GridViewWidget::RENDER_PAGE_SIZE,
        GridViewWidget::RENDER_SEARCH_INPUT,
    ],
    'generate'            => $generate,
    'filename'            => 'user_backoffice.csv',
    'appendRouteSettings' => 'backoffice',
    'columns'             => [
        'id',
        'username',
        'email:email',
        [
            'attribute' => 'roleName',
            'value'     => function (User $data) {
                $rolesName = [];
                foreach ($data->usersRoles as $usersRole) {
                    $rolesName[] = $usersRole->role->name;
                }
                return join('<br/>', $rolesName);
            },
            'filter'    => ArrayHelper::map($roles, 'id', 'name'),
        ],
        [
            'attribute' => 'status',
            'filter'    => User::getStatuses(),
            'value'     => function (User $data) {
                return $data->getStatusLabel();
            },
        ],
        [
            'class'    => 'yii\grid\ActionColumn',
            'header'   => 'Действия',
            'template' => '{view} ' .
                (\Yii::$app->user->can(Permission::BACKOFFICE_USER_UPDATE) ? '{update} ' : '')
        ],
    ],
]);