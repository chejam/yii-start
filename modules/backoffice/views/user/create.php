<?php
/**
 * @var yii\web\View $this
 * @var \app\models\backoffice\forms\UserForm $user
 * @var \app\models\Role[] $roles
 */

$this->title = 'Новый пользователь Backoffice';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('form/_form', [
        'user' => $user,
        'roles' => $roles,
        'isNewRecord' => true,
    ]) ?>

</div>
