<?php
/**
 * @var \yii\web\View $this
 * @var ActiveForm $form
 * @var \app\models\backoffice\forms\UserForm $user
 */

use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
\app\assets\backoffice\BackofficeLoginAsset::register($this);
\app\assets\AppAsset::register($this);

$message = Yii::$app->session->getFlash('login-info');
?>

<div class="user-create">
    <?= $this->render('form/_formUser', [
        'user' => $user,
        'isNewRecord' => true,
    ]) ?>
</div>