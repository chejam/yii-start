<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\backoffice\forms\UserForm $user
 * @var ActiveForm $form
 * @var \app\models\Role[] $roles
 * @var bool $isNewRecord
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

\app\assets\AppAsset::register($this);
$isSelfUserForm = \Yii::$app->user->id == $user->id;
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-7 col-md-8">
            <?= $form->field($user, 'surname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($user, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($user, 'patronymic')->textInput(['maxlength' => true]) ?>
            <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($user, 'phone1')->textInput(['maxlength' => true]) ?>
            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($user, 'phone2')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($user, 'phone2_ext')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <?= $form->field($user, 'role_id')->dropDownList(ArrayHelper::map($roles, 'id', 'name'), ['disabled' => $isSelfUserForm]) ?>
            <?php if (!$isNewRecord) { ?>
                <?= $form->field($user, 'status')->dropDownList(\app\models\User::getStatuses(), ['disabled' => $isSelfUserForm]) ?>
                <?= $form->field($user, 'password')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($user, 'passwordConfirmation')->passwordInput(['maxlength' => true]) ?>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($isNewRecord ? 'Создать пользователя' : 'Обновить', ['class' => $isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>