<?php
/**
 * @var yii\web\View $this
 * @var \app\models\backoffice\forms\UserForm $user
 * @var \yii\widgets\ActiveForm $form
 * @var \app\models\Role[] $roles
 * @var bool $isNewRecord
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

\app\assets\AppAsset::register($this);
$isSelfUserForm = \Yii::$app->user->id == $user->id;
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Bablocloud</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Регистрация пользователя</p>
        <div class="user-form">
            <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>
                <?= $form->field($user, 'password')->passwordInput(['maxlength' => true]) ?>
                <?= $form->field($user, 'passwordConfirmation')->passwordInput(['maxlength' => true]) ?>
            <div class="form-group">
                <?= Html::submitButton($isNewRecord ? 'Создать пользователя' : 'Обновить', ['class' => $isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <a role="button" href="<?= \yii\helpers\Url::to(['user/login']) ?>">Вернуться
                на главную</a>
        </div>
    </div>
</div>