<?php
/**
 * @var \yii\web\View $this
 * @var User $searchModel
 * @var ActiveDataProvider $dataProvider
 * @var Role[] $roles
 */

use app\models\Role;
use app\models\User;
use app\widgets\actionButton\ActionButtonsWidget;
use app\models\Permission;
use yii\data\ActiveDataProvider;


$this->title = 'Список пользователей Backoffice';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">
    <?= $this->render(
        '_gridView',
        [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
            'roles'        => $roles,
            'generate'     => false,
        ]
    ); ?>
    <p>
        <?= ActionButtonsWidget::widget([
            'buttons' => [
                [
                    'permission' => Permission::BACKOFFICE_USER_CREATE,
                    'label'      => 'Добавить пользователя',
                    'url'        => ['create'],
                    'options'    => ['class' => 'btn btn-success'],
                ],
            ],
        ]); ?>
    </p>
</div>
