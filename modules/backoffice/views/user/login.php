<?php
/**
 * @var \yii\web\View $this
 * @var ActiveForm $form
 * @var \app\models\backoffice\forms\LoginForm $loginForm
 * @var \app\models\backoffice\forms\RecoveryForm $recoveryForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Вход';
$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
\app\assets\backoffice\BackofficeLoginAsset::register($this);
\app\assets\AppAsset::register($this);

$message = Yii::$app->session->getFlash('login-info');
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Bablocloud</b></a>
    </div>
    <div class="login-box-body">
        <div class="toggle" id="loginform">
            <?php if (isset($message)) { ?>
                <p class="description-block bg-success" style="padding-left: 3px; padding-right: 3px"><?= $message ?></p>
            <?php } else { ?>
                <p class="login-box-msg">Выполните вход для использования приложения</p>
            <?php } ?>

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

            <?= $form
                ->field($loginForm, 'login', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $loginForm->getAttributeLabel('login')]) ?>

            <?= $form
                ->field($loginForm, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $loginForm->getAttributeLabel('password')]) ?>

            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($loginForm, 'rememberMe')->checkbox() ?>
                    <a class="toggle-button" role="button" id="lostbutt">Забыли пароль?</a>
                    <a role="button" href="<?= \yii\helpers\Url::to(['user/register']) ?>">Зарегистрироваться</a>
                </div>
                <div class="col-xs-4">
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="toggle hide" id="lostform">
            <?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::to(['user/recovery'])]) ?>
            <?= $form->field($recoveryForm, 'email')->textInput() ?>
            <button class="toggle-button btn btn-default btn-flat" id="cancbutt">Отмена</button>
            <?= Html::submitButton('Восстановить', ['class' => 'btn btn-primary btn-flat']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>