<?php
/**
 * @var \app\models\backoffice\forms\ResetTokenPasswordForm $resetForm
 */

use yii\bootstrap\ActiveForm;

$message = Yii::$app->session->getFlash('login-error');
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Visa Portal</b></a>
    </div>
    <div class="login-box-body">
        <?php if (isset($message)) { ?>
            <p class="login-box-msg"><?= $message ?></p>
            <div class="text-center">
                <a class="btn btn-default btn-flat" role="button" href="<?= \yii\helpers\Url::to(['login']) ?>">Вернуться
                    на главную</a>
            </div>
        <?php } else { ?>
            <?php $form = ActiveForm::begin() ?>
            <?= $form->field($resetForm, 'password')->textInput(['type' => 'password']) ?>
            <?= $form->field($resetForm, 'passwordConfirmation')->textInput(['type' => 'password']) ?>
            <button class="btn btn-primary btn-flat">Продолжить</button>
            <?php ActiveForm::end() ?>
        <?php } ?>
    </div>
</div>