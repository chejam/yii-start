<?php
/**
 * @var yii\web\View $this
 * @var \app\models\backoffice\forms\UserForm $user
 * @var \app\models\Role[] $roles
 */

$this->title = 'Редактирование пользователя Backoffice: ' . $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-update">
    <?= $this->render('form/_form', [
        'user' => $user,
        'roles' => $roles,
        'isNewRecord' => false,
    ]) ?>
</div>
