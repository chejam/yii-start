<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\User $user
 */

use app\widgets\actionButton\ActionButtonsWidget;
use app\models\Permission;
use yii\widgets\DetailView;


$this->title = $user->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <?= DetailView::widget([
    'model' => $user,
        'attributes' => [
            'id',
            'surname',
            'name',
            'patronymic',
            'email:email',
            'phone1',
            [
                'attribute' => 'phone2',
                'value' => function(\app\models\User $data) {
                    return $data->phone2 ? ($data->phone2 . ($data->phone2_ext ? ' (доб. ' . $data->phone2_ext . ')' : '')) : '';
                },
            ],
            [
                'attribute' => 'roleName',
                'value' => function(\app\models\User $data) {
                    $rolesName = [];
                    foreach ($data->usersRoles as $usersRole) {
                        $rolesName[] = $usersRole->role->name;
                    }
                    return join('<br/>', $rolesName);
                },
            ],
            [
                'label' => 'Статус',
                'value' => $user->getStatusLabel(),
            ],
            'create_time:datetime',
            'update_time:datetime',
        ],
    ]) ?>
    <p>
        <?= ActionButtonsWidget::widget([
            'buttons' => [
                [
                    'permission' => Permission::BACKOFFICE_USER_UPDATE,
                    'label' => 'Редактировать',
                    'url' => ['update', 'id' => $user->id],
                    'options' => ['class' => 'btn btn-primary'],
                ],
            ]
        ]); ?>
    </p>

</div>
