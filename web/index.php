<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', isset($_SERVER['YII_DEBUG']) ? filter_var($_SERVER['YII_DEBUG'], FILTER_VALIDATE_BOOLEAN) : false);
defined('YII_ENV') or define('YII_ENV', $_SERVER['YII_ENV'] ?? 'dev');
$yiiActiveApp = $_SERVER['YII_ACTIVE_APP'] ?? 'backoffice';

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/../config/web.php'), require(__DIR__ . '/../config/' . $yiiActiveApp . '.php'));
require(__DIR__ . '/../config/dependencies.php');

(new yii\web\Application($config))->run();
