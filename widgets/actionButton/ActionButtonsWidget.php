<?php

namespace app\widgets\actionButton;

use yii\base\Widget;
use yii\helpers\Html;

class ActionButtonsWidget extends Widget
{
    /** @var array */
    public $buttons;

    /**
     * @inheritdoc
     */
    public function run() : string
    {
        $buttons = [];
        foreach ($this->buttons as $button) {
             $buttons[] = $this->renderButton($button);
        }
        return implode("\n", $buttons);
    }

    /**
     * @param array $button
     * @return string
     */
    private function renderButton(array $button) : string
    {
        if (isset($button['permission']) && !\Yii::$app->user->can($button['permission'])) {
            return '';
        }
        return Html::a($button['label'], $button['url'], $button['options']);
    }
}