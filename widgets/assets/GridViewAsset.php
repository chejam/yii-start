<?php

namespace app\widgets\assets;
use app\assets\JQueryUIAsset;
use yii\web\AssetBundle;

class GridViewAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/assets/gridview';
    public $css = [
    ];
    public $js = [
        'js/settings.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        JQueryUIAsset::class,
    ];
}
