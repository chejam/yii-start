$(document).ready(function () {
    initPopover($('#grid-view-settings-btn'));



    $(document)
        .on('keyup', '[name="full-search"]', function (event) {
            if (event.keyCode == 13) {
                $(this).closest('form').submit();
            }
        })
        .on('change', '[name="per-page"]', function () {
            window.location = $(this).find('option:selected').data('url');
        });
});

function initPopover(popoverTrigger) {
    var popover;
    var form;
    var checkBoxes;
    popoverTrigger.popover().on('shown.bs.popover',function () {
        var obj = $(this);
        obj.addClass('active');
        popover = $(obj).closest('.grid-view').find('.popover');
        form = popover.find('[role="form"]');
        form.find('[rel="sortable"]').sortable();
        popover.find('[rel="cancel"]').click(function () {
            obj.popover('hide');
        });
        checkBoxes = form.find('input[type=checkbox]');

        form.on('change', 'input[type=checkbox]', function() {
            if (form.find('input[type=checkbox]:checked').length == 0) {
                $(this).prop('checked', true);
            }
        });
        popover.find('[rel="submit"]').click(function () {
            checkBoxes.each(function () {
                if ($(this).prop('checked')) {
                    $(this).next().prop('disabled', true);
                } else {
                    $(this).next().prop('disabled', false);
                }
            });
            $.post({
                url: form.data('url'),
                data: form.find('input').serializeArray(),
                success: function() {
                    location.reload();
                }
            });
        });
    }).on('hidden.bs.popover',function () {
        var obj = $(this);
        obj.removeClass('active');
    }).attr('title', popoverTrigger.data('original-title'));
}
