<?php

namespace app\widgets\gridView;

use app\components\managers\interfaces\UserGridViewSettingsManagerInterface;
use yii\base\InvalidConfigException;
use yii\grid\ActionColumn;
use yii\grid\Column;
use yii\grid\DataColumn;
use yii\grid\SerialColumn;

/**
 * Class ColumnOrderGridViewTrait
 */
trait ColumnOrderGridViewTrait
{
    /** @var UserGridViewSettingsManagerInterface */
    protected $gridViewSettingsManager;
    /** @var DataColumn[] */
    private $dataColumns = [];
    /** @var array|null */
    private $columnsSettings = null;

    public $appendRouteSettings = '';

    protected function reorderColumns()
    {
        $columnsSettings = $this->getColumnsSettings();
        if ($columnsSettings) {
            $params = [];
            foreach ($columnsSettings as $columnsSetting) {
                $params[$columnsSetting['name']] = $columnsSetting['visible'] ?? '';
            }
            foreach ($this->columns as $key => $column) {
                if ($column instanceof DataColumn && !isset($params[$column->attribute])) {
                    $columnsSettings[] = [
                        'name'    => $column->attribute,
                        'visible' => true,
                    ];
                }
            }

            $reorderedColumns = [];

            if ($serialColumn = $this->findSerialColumn()) {
                $reorderedColumns[] = $serialColumn;
            }

            foreach ($columnsSettings as $settings) {
                if ($settings['visible'] && $column = $this->findColumn($settings['name'] ?? '')) {
                    $reorderedColumns[] = $column;
                }
            }

            if ($actionColumn = $this->findActionColumn()) {
                $reorderedColumns[] = $actionColumn;
            }

            $this->columns = $reorderedColumns;
        }
    }

    /**
     * @return array
     */
    private function getColumnsSettings() : array
    {
        if ($this->columnsSettings === null) {
            $routeSettings = \Yii::$app->request->pathInfo . $this->appendRouteSettings;
            if (\Yii::$app->request->get('routeSettings', false)) {
                $routeSettings = \Yii::$app->request->get('routeSettings');
            }

            $this->columnsSettings = $this->gridViewSettingsManager->getSettings(
                \Yii::$app->user->getId(),
                $routeSettings,
                $this->id
            );

            if (!empty($this->columnsSettings)) {
                foreach ($this->columns as $column) {
                    $exist = false;
                    foreach ($this->columnsSettings as $key => $columnsSetting) {
                        if ($columnsSetting['name'] == ($column->attribute ?? '')) {
                            $exist = true;
                            break;
                        }
                    }
                    if (!$exist) {
                        $this->columnsSettings[] = [
                            'name'    => $column->attribute ?? '',
                            'visible' => true,
                        ];
                    }
                }
            }
        }
        return $this->columnsSettings;
    }

    /**
     * @return array
     */
    private function createEmptyColumnsSettings() : array
    {
        $columnsSettings = [];
        foreach ($this->columns as $column) {
            if ($column instanceof DataColumn) {
                $settings['name'] = $column->attribute;
                $settings['visible'] = true;
                $columnsSettings[] = $settings;
            }
        }
        return $columnsSettings;
    }

    /**
     * @param string $columnName
     * @return Column
     * @throws InvalidConfigException
     */
    private function findColumn(string $columnName)
    {
        foreach ($this->columns as $column) {
            if ($column instanceof DataColumn && $column->attribute == $columnName) {
                return $column;
            }
        }
        return null;
    }

    /**
     * @return SerialColumn|null
     */
    private function findSerialColumn()
    {
        foreach ($this->columns as $column) {
            if ($column instanceof SerialColumn) {
                return $column;
            }
        }
        return null;
    }

    /**
     * @return ActionColumn|null
     */
    private function findActionColumn()
    {
        foreach ($this->columns as $column) {
            if ($column instanceof ActionColumn) {
                return $column;
            }
        }
        return null;
    }

}