<?php

namespace app\widgets\gridView;

use app\components\managers\interfaces\UserGridViewSettingsManagerInterface;
use app\models\search\OrderSearch;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\grid\DataColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Request;

/**
 * Class GridViewWidget
 */
class GridViewWidget extends GridView
{
    use ColumnOrderGridViewTrait;

    const RENDER_SETTINGS_BUTTON = 'renderSettingsButton';
    const RENDER_DOWNLOAD_BUTTON = 'renderDownloadButton';
    const RENDER_UPLOAD_BUTTON   = 'renderUploadButton';
    const RENDER_SEARCH_INPUT    = 'renderSearchInput';
    const RENDER_FILTER_DATE     = 'renderFilterDate';
    const RENDER_PAGE_SIZE       = 'renderPageSize';

    private $renderAllButtons = [
        self::RENDER_SETTINGS_BUTTON,
        self::RENDER_DOWNLOAD_BUTTON,
        self::RENDER_UPLOAD_BUTTON,
    ];

    private $renderAllInputs = [
        self::RENDER_SEARCH_INPUT,
        self::RENDER_FILTER_DATE,
        self::RENDER_PAGE_SIZE,
    ];

    public $summaryOptions = ['class' => 'summary col-md-9'];

    public $useCustomColumnsSettings = true;

    public $renderButtons = [self::RENDER_SETTINGS_BUTTON];

    public $generate = false;

    public $download = true;

    public $saveDir = "";

    public $filename = "export.csv";

    public $delimiter = ";";

    public function __construct(UserGridViewSettingsManagerInterface $gridViewSettingsManager, array $config = [])
    {
        $this->gridViewSettingsManager = $gridViewSettingsManager;
        parent::__construct($config);
    }

    public function initColumns()
    {
        parent::initColumns();
        $this->reorderColumns();
    }

    /**
     * @inheritdoc
     */
    public function renderSummary() : string
    {
        $rowInput = ($this->useCustomColumnsSettings ? $this->renderInputsRow() : '');
        $content = parent::renderSummary() . ($this->useCustomColumnsSettings ? $this->renderButtonsRow() : '');
        $rowContent = Html::tag('div', $content, ['class' => 'row']);
        return $rowInput . $rowContent;
    }

    private function renderButtonsRow() : string
    {
        $buttons = [];
        foreach ($this->renderButtons as $renderButton) {
            if (in_array($renderButton, $this->renderAllButtons)) {
                $buttons[] = $this->$renderButton();
            }
        }

        return Html::tag('div', join(' ', $buttons), ['class' => 'col-md-3 text-right pull-right']);
    }

    private function renderInputsRow() : string
    {
        $cols = [];
        foreach ($this->renderButtons as $renderCols) {
            if (in_array($renderCols, $this->renderAllInputs)) {
                $class = 'col-md-6';
                if (in_array($renderCols, [self::RENDER_SEARCH_INPUT, self::RENDER_FILTER_DATE])) {
                    $class .= ' text-right pull-right';
                }
                $cols[] = Html::tag('div', $this->$renderCols(), ['class' => $class]);
            }
        }
        $row = !empty($cols) ? Html::tag('div', join(' ', $cols), ['class' => 'row']) : '';

        return $row;
    }

    private function renderSettingsButton() : string
    {
        $buttonContent = Html::tag('span', '', ['class' => 'fa fa-gear', 'id']);
        $checkboxes = $this->renderCheckboxes();
        $popoverContent = $checkboxes; //Html::tag('div', '', $options = ['']);
        $button = Html::tag(
            'button',
            $buttonContent,
            [
                'type' => 'button',
                'class' => 'btn btn-default',
                'id' => 'grid-view-settings-btn',
                'data-viewport' => "#{$this->id}",
                'data-toggle' => 'popover',
                'data-content' => $popoverContent,
                'data-placement' => 'left',
                'data-html' => 1,
                'title' => 'Настройки отображения колонок',
                'max-width' => '100%',
            ]
        );
        return $button;
    }

    private function renderDownloadButton() :string
    {
        $urlParams = \Yii::$app->getRequest()->get();
        if (empty($urlParams)) {
            $urlParams = [];
        }
        $urlParams['routeSettings'] = \Yii::$app->request->pathInfo;
        $button = Html::a(
            Html::tag('span', '', ['class' => 'fa fa-download']),
            array_merge(['download'], $urlParams),
            [
                'class'          => 'btn btn-default',
                'id'             => 'grid-view-download-btn',
                'data-viewport'  => "#{$this->id}",
                'data-placement' => 'left',
                'data-html'      => 1,
                'title'          => 'Скачать csv файл',
                'max-width'      => '100%',
                'target'         => '_blank',
            ]
        );
        return $button;
    }

    private function renderUploadButton() : string
    {
        $button = Html::a(
            Html::tag('span', '', ['class' => 'fa fa-upload']),
            ['upload'],
            [
                'type' => 'button',
                'class' => 'btn btn-default',
                'id' => 'grid-view-download-btn',
                'data-viewport' => "#{$this->id}",
                'data-placement' => 'left',
                'data-html' => 1,
                'title' => 'Импортировать данные',
                'max-width' => '100%',
            ]
        );
        return $button;
    }

    private function renderSearchInput() : string
    {
        $request = \Yii::$app->getRequest();
        $params = $request instanceof Request ? $request->getQueryParams() : [];
        $value = !empty($params['full-search']) ? $params['full-search'] : '';
        $params[0] = \Yii::$app->controller->getRoute();

        $searchInput =
            Html::beginForm(Url::to(), 'get') .
            Html::tag('div',
                Html::tag('label', 'Поиск ') . ' ' .
                $button = Html::textInput('full-search', $value, ['class' => 'form-control']),
                ['class' => 'form-inline']
            ) .
            Html::endForm();
        return $searchInput;
    }

    private function renderFilterDate() : string
    {
        $request = \Yii::$app->getRequest();
        $params = $request instanceof Request ? $request->getQueryParams() : [];
        $dateStart = $this->filterModel->date_start ?? '';
        $dateEnd = $this->filterModel->date_end ?? '';
        $params[0] = \Yii::$app->controller->getRoute();

        $searchInput =
            Html::beginForm(Url::to(), 'get') .
            Html::tag('div',
                Html::tag('label', 'Дата') . ' с ' .
                DatePicker::widget([
                    'name'       => Html::getInputName($this->filterModel, 'date_start'),
                    'value'      => $dateStart,
                    'class'      => 'form-control',
                    'language'   => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options'    => ['class' => 'form-control'],
                ]) . ' по ' .
                DatePicker::widget([
                    'name'       => Html::getInputName($this->filterModel, 'date_end'),
                    'value'      => $dateEnd,
                    'class'      => 'form-control',
                    'language'   => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options'    => ['class' => 'form-control'],
                ]) . ' ' .
                Html::button('Отфильтровать', ['type' => 'submit', 'class' => 'btn btn-default']),
                ['class' => 'form-inline']
            ) .
            Html::endForm();
        return $searchInput;
    }

    private function renderPageSize() : string
    {
        $pages = [10, 20, 50, 100];
        $list = [];
        $request = \Yii::$app->getRequest();
        $params = $request instanceof Request ? $request->getQueryParams() : [];
        $selected = !empty($params['per-page']) && $params['per-page'] > 0 ? $params['per-page'] : 10;
        $params[0] = \Yii::$app->controller->getRoute();
        $urlManager = \Yii::$app->getUrlManager();
        $options = [];
        foreach ($pages as $page) {
            $params['per-page'] = $page;
            $list[$page] = $page;
            $options[$page] = ['data-url' =>  $urlManager->createUrl($params)];
        }

        $pageSize =
            Html::tag('div',
            Html::tag('label', 'Показывать по ') . ' ' .
            $button = Html::dropDownList('per-page', $selected, $list, ['class' => 'form-control', 'options' => $options]) . ' ' .
            Html::tag('label', 'записей'),
            ['class' => 'form-inline']
        );
        return $pageSize;
    }

    private function renderCheckboxes()
    {
        $content = Html::beginTag('div', ['role' => 'form', 'data-url' => Url::to('/backoffice/user/grid-view-settings'), 'data-grid-view-id' => $this->id]);

        $columnsSettings = $this->getColumnsSettings();
        if (!$columnsSettings) {
            $columnsSettings = $this->createEmptyColumnsSettings();
        }
        $content .= Html::hiddenInput('grid_id', $this->id);
        $content .= Html::hiddenInput('route', \Yii::$app->request->pathInfo . ($this->appendRouteSettings ?? ''));
        $content .= Html::beginTag('region', ['rel' => 'sortable']);
        /** @var ActiveRecord $model */
        $model = new $this->dataProvider->query->modelClass;
        foreach ($columnsSettings as $settings) {
            $elementName = 'params[' . $settings['name'] . ']';
            $content .= Html::beginTag('div', ['class' => 'checkbox']);
            $content .= Html::beginTag('label');
            $content .= Html::checkbox($elementName, $settings['visible'], ['value' => 1]);
            $content .= Html::hiddenInput($elementName, 0);
            $content .= $model->getAttributeLabel($settings['name']);
            $content .= Html::endTag('label');
            $content .= Html::endTag('div');
        }
        $content .= Html::endTag('region');
        $content .= Html::beginTag('div', ['class' => 'btn-toolbar']);
        $content .= Html::button('Применить', ['class' => 'btn btn-primary', 'rel' => 'submit',]);
        $content .= Html::button('Отмена', ['class' => 'btn btn-default', 'rel' => 'cancel']);
        $content .= Html::endTag('div');

        $content .= Html::endForm();
        return $content;
    }

    public function run()
    {
        if ($this->generate) {
            $this->generateCsv();
        } else {
            $request = \Yii::$app->getRequest();
            if ($this->dataProvider->getPagination()) {
                $params = $request instanceof Request ? $request->getQueryParams() : [];
                $this->dataProvider->getPagination()->setPageSize($params['per-page'] ?? 10);
            }
            parent::run();
        }
    }

    public function generateCsv()
    {
        if(!$this->saveDir) {
            $this->saveDir = \Yii::$app->basePath."/runtime";
        }
        if(!file_exists($this->saveDir)) {
            mkdir($this->saveDir, 0777, true);
        }
        $filename = $this->filename;

        if($this->download){
            $fp = fopen('php://output', 'w');
            header("Content-type: application/vnd.ms-excel;charset=utf-8");
            header( 'Content-Disposition: attachment;filename='.$filename);
        }else{
            $fp = fopen($this->saveDir."/".$this->filename, 'w');
        }
        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = $this->dataProvider;
        $dataProvider->pagination->setPageSize(0);

        $models = array_values($dataProvider->getModels());
        $headerData = [];
        foreach ($this->columns as $column) {
            if ($column instanceof DataColumn) {
                $headerData[] = mb_convert_encoding(strip_tags($column->renderHeaderCell()), 'Windows-1251', 'UTF-8');
            }
        }
        fputcsv($fp, $headerData, $this->delimiter);

        if (!empty($models)) {
            $keys = $this->dataProvider->getKeys();
            foreach ($models as $index => $model) {
                $key = $keys[$index];

                $rowData = [];
                foreach ($this->columns as $column) {
                    if ($column instanceof DataColumn) {
                        $rowData[] = mb_convert_encoding($column->getDataCellValue($model, $key, $index), 'Windows-1251', 'UTF-8');
                    }
                }
                fputcsv($fp, $rowData, $this->delimiter);
            }
        }

        fclose($fp);
    }
}